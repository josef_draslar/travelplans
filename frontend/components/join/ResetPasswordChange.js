import React from "react";
import PropTypes from "prop-types";
import axios from "../../utils/Axios";
import styled from "styled-components";
import { Form, FormFeedback, FormGroup, Input, ModalHeader } from "reactstrap";
import Button from "../Button";
import { ButtonWrapper, LabelLocal } from "./ModalJoinElements";
import { pushNotificationMessageIfPossible } from "../NotificationMessage";
import { getParameterFromUrl, removeParamsFromUrl } from "../../utils/Utils";

//TODO finish implementaion
class ResetPasswordChange extends React.PureComponent {
  state = {
    password: "",
    accessToken: null,
    error: "",
    isLoading: false
  };

  componentDidMount() {
    const access_token = "access_token";
    const accessTokenValue = getParameterFromUrl(access_token);
    if (accessTokenValue) {
      this.setState({ accessToken: accessTokenValue });
      removeParamsFromUrl(access_token);
    }
  }

  onChange = e => {
    const { name, value } = e.target;

    this.setState({
      [name]: value,
      error: ""
    });
  };

  onSubmit = e => {
    e.preventDefault();
    const { password, accessToken } = this.state;
    const { close } = this.props;

    if (!accessToken) {
      this.setState({
        error: "Špatné url, neobsahuje všechny potřebné prvky."
      });
      return;
    }
    this.setState({ isLoading: true });
    axios
      .post("/api/users/reset-password?access_token=" + accessToken, {
        newPassword: password
      })
      .then(() => {
        this.setState({ isLoading: false });
        close();
        pushNotificationMessageIfPossible({
          message: "Heslo úspěšně změněno, nyní se můžete přihlásit.",
          type: "SUCCESS"
        });
      })
      .catch(({ response }) => {
        if (
          response.data &&
          response.data.error &&
          response.data.error.details &&
          response.data.error.details.codes
        ) {
          this.setState({
            errors: response.data.error.details.codes,
            isLoading: false
          });
        }
      });
  };

  isDisabled = () => {
    const { password } = this.state;

    return !password;
  };

  render() {
    const { password, isLoading } = this.state;
    return (
      <div>
        <ModalHeader>Změna hesla</ModalHeader>
        <div className="row">
          <EmailWrapper>
            <Form onSubmit={this.onSubmit}>
              <FormGroup>
                <LabelLocal htmlFor="password-field">HESLO</LabelLocal>
                <Input
                  id="password-field"
                  type="password"
                  name="password"
                  value={password}
                  onChange={this.onChange}
                  invalid={!!this.state.error}
                />
                <FormFeedback>{this.state.error}</FormFeedback>
              </FormGroup>
              <ButtonWrapper>
                <Button
                  disabled={this.isDisabled()}
                  isLoading={isLoading}
                  type="submit"
                  isButton
                >
                  Změň heslo
                </Button>
              </ButtonWrapper>
            </Form>
          </EmailWrapper>
        </div>
      </div>
    );
  }
}

ResetPasswordChange.propTypes = {
  close: PropTypes.func.isRequired
};

export default ResetPasswordChange;

const EmailWrapper = styled.div`
  margin-right: auto;
  margin-left: auto;
  max-width: 280px;
`;
