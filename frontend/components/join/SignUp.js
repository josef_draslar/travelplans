import React, {useState} from "react";
import styled from "styled-components";
import {Alert, Form, FormGroup, Input} from "reactstrap";
import PropTypes from "prop-types";
import Button from "../Button";
import { pushNotificationMessageIfPossible } from "../NotificationMessage";
import {
    ButtonWrapper,
    LabelLocal,
    Or,
    SmallText,
    SmallTextSpan,
    Text,
    EmailWrapper,
    A,
    FormWrapper
} from "./ModalJoinElements";
import fonts from "../../utils/fonts";
import {FacebookButton} from "./FacebookButton";
import {ModalHeader} from "../navbar/ModalHeader";
import {withTranslation} from "../../utils/multiLanguageSupport";
import {useDispatch} from "react-redux";
import {initLoading} from "../../utils/Utils";
import { createUser } from "../../redux/actions/UserActions";

const SignUpForm = ({ t }) => {
    const [form, setForm] = useState({
        user: "",
        password: ""
    });
    const { email, password } = form;
    const [isLoading, setLoading] = useState(false);
    const dispatch = useDispatch();
    const onComplete = p => {
        if(p===false)
            pushNotificationMessageIfPossible({
                title: t("Join.registrationSuccess"),
                message: t("Join.youReLogged"),
                type: "SUCCESS"
            });
        setLoading(p);
    };

    const onChange = e => {
        const { name, value } = e.target;
        setForm({ ...form, [name]: value });
        typeof isLoading === "string" && setLoading(false);
    };

    const onSubmit = e => {
        e.preventDefault();

        dispatch(
            createUser(
                {
                    email,
                    password
                },
                initLoading(onComplete)
            )
        );
    };

    const isDisabled = () => !(email && password && password.length > 5);

    return (
        <Form onSubmit={onSubmit}>
            <FormGroup>
                <LabelLocal htmlFor="email-field">
                    {t("Join.email").toUpperCase()}
                </LabelLocal>
                <Input
                    id="email-field"
                    type="text"
                    name="email"
                    value={email}
                    onChange={onChange}
                />
            </FormGroup>
            <FormGroup>
                <LabelLocal htmlFor="password-field">
                    {t("Join.password").toUpperCase()}
                </LabelLocal>
                <Input
                    id="password-field"
                    type="password"
                    name="password"
                    value={password}
                    onChange={onChange}
                />
            </FormGroup>
            {typeof isLoading === "string" &&
            <Alert color="danger">
                {isLoading}
            </Alert>}
            <ButtonWrapper>
                <Button
                    disabled={isDisabled()}
                    isLoading={isLoading === true}
                    type="submit"
                    isButton
                >
                    {t("NavBar.signUp").toUpperCase()}
                </Button>
            </ButtonWrapper>
        </Form>
    );
};

SignUpForm.propTypes = {
    t: PropTypes.object.isRequired
};

const SignUp = ({t, swap, allowFacebook}) => {
    return (
      <div>
          <ModalHeader>{t("NavBar.signUp")}</ModalHeader>
          {allowFacebook ?
              <div className="row">
                <div className="col-sm-6">
                  <H6>{t("Join.withSocialMedia")}</H6>
                    <FacebookButton />
                </div>
                <div className="col-sm-6">
                  <EmailWrapper>
                      <Or>
                          {t("Base.or")}
                      </Or>
                      <H6>
                          {t("Join.withEmail")}
                      </H6>
                      <SignUpForm t={t}/>
                  </EmailWrapper>
                </div>
              </div>
              :
              <FormWrapper>
                  <SignUpForm t={t}/>
              </FormWrapper>
          }

          <Text>
              {t("Join.alreadyHaveAccount")} <A onClick={swap}>{t("NavBar.signIn")}</A>
          </Text>
        <SmallText>
          <SmallTextSpan>
              {t("Join.iAcknowledge")}{" "}
            <A
              href="/info/terms-of-service"
              target="_blank"
            >
                {t("Join.terms")}
            </A>{" "}
              {t("Base.and")}{" "}
            <A href="/info/privacy-policy" target="_blank">
                {t("Join.policy")}
            </A>
          </SmallTextSpan>
        </SmallText>
      </div>
    );
  };

SignUp.propTypes = {
    allowFacebook: PropTypes.bool
};

export default withTranslation(SignUp);

export const H6 = styled.h6`
  text-align: center;
  font-size: inherit;
  font-family: ${fonts.title};
`;
