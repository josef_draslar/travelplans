import React from "react";
import { NavItem, NavLink } from "reactstrap";
import styled, { css } from "styled-components";
import PropTypes from "prop-types";

import colors from "../../utils/colors";
import { media } from "../../utils/Media";
import fonts from "../../utils/fonts";
import { withTranslation } from "../../utils/multiLanguageSupport";
import Link from "next/link";
import { useRouter } from "next/router";

const NavItemInner = ({
  onClick,
  name,
  isPrimary,
  url,
  hideForMobile,
  hide,
  isMobile,
  isInlined,
  translated,
  navBarWhite,
  t
}) => {
  const router = useRouter();
  const location = router.pathname;
  if (url) {
    return (
      <NavItemMainLocal
        hideForMobile={hideForMobile}
        hide={hide}
        isPrimary={isPrimary}
        isInlined={isInlined}
      >
        <Link href={url}>
          <NavLinkStyled
            isPrimary={isPrimary}
            className={url === location ? "isActive" : ""}
            onClick={onClick}
            isMobile={isMobile}
            navBarWhite={navBarWhite}
          >
            {translated ? name : t("NavBar." + name)}
          </NavLinkStyled>
        </Link>
      </NavItemMainLocal>
    );
  } else {
    return (
      <NavItemMainLocal
        isPrimary={isPrimary}
        isInlined={isInlined}
        hideForMobile={hideForMobile}
        hide={hide}
      >
        <NavAStyled
          isPrimary={isPrimary}
          onClick={onClick}
          isMobile={isMobile}
          navBarWhite={navBarWhite}
        >
          {translated ? name : t("NavBar." + name)}
        </NavAStyled>
      </NavItemMainLocal>
    );
  }
};

NavItemInner.propTypes = {
  name: PropTypes.string.isRequired,
  translated: PropTypes.bool,
  onClick: PropTypes.func,
  url: PropTypes.string,
  isPrimary: PropTypes.bool,
  hideForMobile: PropTypes.bool,
  hide: PropTypes.number,
  isMobile: PropTypes.bool,
  isInlined: PropTypes.bool,
  navBarWhite: PropTypes.bool
};

export const NavigationItem = withTranslation(NavItemInner);

const navLinkBase = css`
    background: none;
    border: none;
    position: relative;
    font-size: 17px;
    line-height: 22px;
    color: ${props => (props.navBarWhite ? colors.white : colors.font)};
    font-family: ${fonts.title};
    letter-spacing: .1em;
    text-transform: uppercase;
        padding: 44px 10px 42px 10px;
    
    -webkit-transition: color .2s ease-out;
    -moz-transition: color .2s ease-out;
    transition: color .2s ease-out;
    
    ${props =>
      props.isMobile &&
      `
        padding-top: 15px;
        padding-bottom: 15px;
    `}
    
    &:hover,
    &:focus,
    &.isActive {
        text-decoration: none;
        color: ${colors.primary} !important;
    }
    
    &:hover,
    &:focus{
        cursor: pointer;
    }
    
  ${props =>
    props.isPrimary &&
    css`
      border: 1px solid ${colors.primary};
      color: ${colors.primary};

      &.isActive,
      &:hover,
      &:focus {
        color: ${colors.white};
        background-color: ${colors.primaryHover};
      }
    `};
`;

export const NavLinkStyled = styled(({ isPrimary, isMobile, ...rest }) =>
  <NavLink {...rest} />
)`
  ${navLinkBase}
`;

export const NavAStyled = styled.button`${navLinkBase};`;

export const NavItemMainLocal = styled(
  ({ isPrimary, hideForMobile, hide, isInlined, ...rest }) =>
    <NavItem {...rest} />
)`
  position: relative;

  height: 100%;

  ${props =>
    props.hideForMobile &&
    css`
      ${media.lessThan("sm")`
          display: none !important;
      `};
    `};

  ${props =>
    !!props.hide &&
    css`
      ${media.lessThan(props.hide + "px")`
          display: none !important;
      `};
    `};

  ${props =>
    props.isPrimary &&
    css`
      padding-left: 20px;
      padding-right: 20px;
    `};

  ${props =>
    props.isInlined &&
    css`
      display: inline-block;
    `};
`;

export const NavItemMobileLocal = styled(NavItem)`
  color: ${colors.primary};

  & > a {
    position: relative;
    width: auto;
    display: inline-block;
    height: 100%;
    line-height: 50px;
    padding: 0 0.5em 0 24px;
    padding-left: 24px !important;
  }

  & > a.isActive {
    color: ${colors.font} !important;
  }

  & > a.isActive:after {
    content: "";
    position: absolute;
    top: 0;
    left: 0;
    display: block;
    width: 9px;
    height: 100%;
    background-color: ${colors.primary};
  }
  
  display: inline;
  width: auto;
  float: left;
`;

export const Title = styled.div`
  font-size: 12px;
  position: relative;
  margin-top: 20px;
  margin-bottom: 10px;
  list-style: none;
  color: rgba(153, 153, 153, 0.5);
  width: 100%;
  letter-spacing: 3px;
  text-transform: uppercase;
  font-weight: 600;
  padding-left: 24px;
`;
