const assert = require('assert')

describe('webdriver.io page', () => {
    it('should have the right title', () => {
        browser.url('/');
        const title = browser.getTitle();
        assert.strictEqual(title, 'Trip planner')
    })

    it('log in as user', () => {
        browser.url('/');
        const button = $('button=Sign in');
        button.click()
        const heading = $('h5=Sign in');
        expect(heading).toBeDefined();
        const emailInput = $('#email-field');
        emailInput.setValue("user@email.com");
        const passwordInput = $('#password-field');
        passwordInput.setValue("123456");
        const buttonSignIn = $('[button.sumbit-button]');
        buttonSignIn.click();
        const buttonLogOut = $('button=Log out');
        expect(buttonLogOut).toBeDefined();

    })
});