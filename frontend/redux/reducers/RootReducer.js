import { combineReducers } from "redux";
import user from "./UserReducer";
import local from "./LocalReducer";

export default combineReducers({
  user,
  local,
});
