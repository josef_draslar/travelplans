const createTrip = `
  mutation CreateTrip($data: TripCreate!){
      createTrip(data: $data){
          id,
          destination,
          startDate,
          endDate,
          comment
      }
    }
`;

const updateTrip = `
  mutation UpdateTrip($data: TripUpdate!){
      updateTrip(data: $data){
          id,
          destination,
          startDate,
          endDate,
          comment
      }
    }
`;

const deleteTrip = `
  mutation DeleteTrip($id: Float!){
      deleteTrip(id: $id)
    }
`;

export const TripMutations = {
    createTrip,
    updateTrip,
    deleteTrip
};