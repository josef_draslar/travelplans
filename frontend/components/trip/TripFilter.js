import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { withTranslation } from "../../utils/multiLanguageSupport";
import { Input } from "reactstrap";
import Button from "../Button";
import { debounce } from "../../utils/Utils";

const TripFilter = ({ t, updateFilters }) => {
  const [destination, setDestination] = useState("");
  const updateFiltersLocal = debounce(updateFilters, 500);
  const updateFiltersLocalNow = debounce(updateFilters, 500, true);
  const onChange = e => {
    setDestination(e.target.value);
    updateFiltersLocal(e.target.value);
  };

  return (
    <Wrapper>
      <InputStyled
        onChange={onChange}
        placeholder={t("Field.destination")}
        type="text"
        value={destination}
      />
      <Button onClick={() => updateFiltersLocalNow(destination)} isButton>
        {t("Trip.search")}
      </Button>
    </Wrapper>
  );
};

TripFilter.propTypes = {
  updateFilters: PropTypes.func.isRequired
};

export default withTranslation(TripFilter);

const Wrapper = styled.div`position: relative;`;

const InputStyled = styled(Input)`
    width: calc(100% - 147px);
    height: 46px;
    border-radius: 0px;
    display: inline-block;
    margin-top: 30px;
`;
