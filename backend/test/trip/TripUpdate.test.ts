import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import faker from "faker";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { stringifyDatesTrip as stringifyDates } from "../test-utils/stringifyDates";
import { Trip, TripUpdate } from "../../src/entity/Trip";
import { ExecutionResult } from "graphql/execution/execute";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { TripMutations } from "../../src/constants-shared/TripMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";

env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const createTripUpdate = (id: number, params: object = {}): TripUpdate => ({
  destination: faker.address.city(),
  comment: faker.lorem.sentence(),
  startDate: faker.date.recent(),
  endDate: faker.date.future(),
  id,
  ...params
});

const updateTrip = async (
  context?: Context,
  expectedErrorMessage?: string,
  trip: TripUpdate = createTripUpdate(defaults.trips.ofCurrent.id),
  tripDbCompareTo: Trip = defaults.trips.ofCurrent
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: TripMutations.updateTrip,
      variableValues: {
        data: trip
      }
    },
    context
  );

  const tripDb = await defaults.repos.trip.findOne({
    where: { id: trip.id },
    relations: ["user"]
  });
  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
    expect(tripDb).toMatchObject(tripDbCompareTo);
  } else {
    trip = {
      comment: tripDbCompareTo.comment,
      destination: tripDbCompareTo.destination,
      endDate: tripDbCompareTo.endDate,
      startDate: tripDbCompareTo.startDate,
      id: tripDbCompareTo.id,
      ...trip
    };

    expect(response.errors).not.toBeDefined();
    expect(response).toMatchObject({
      data: {
        updateTrip: stringifyDates(trip)
      }
    });
    expect(tripDb).toMatchObject(trip);
  }
};

describe("Trip update", () => {
  it("update Trip no auth", async () => {
    await updateTrip(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("update Trip as user", async () => {
    await updateTrip(defaults.contexts.user);
  });

  it("update Trip as manager", async () => {
    await updateTrip(defaults.contexts.manager, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("update Trip of different user as user", async () => {
    await updateTrip(
      defaults.contexts.user,
      ERROR_MESSAGE.ACCESS_DENIED,
      createTripUpdate(defaults.trips.ofDifferent.id),
      defaults.trips.ofDifferent
    );
  });

  it("update Trip of different user as admin", async () => {
    await updateTrip(
      defaults.contexts.admin,
      null,
      createTripUpdate(defaults.trips.ofDifferent.id)
    );
  });

  it("update Trip just startDate", async () => {
    await updateTrip(defaults.contexts.user, null, {
      startDate: faker.date.recent(),
      id: defaults.trips.ofCurrent.id
    });
  });

  it("update Trip just endDate", async () => {
    await updateTrip(defaults.contexts.user, null, {
      endDate: faker.date.future(),
      id: defaults.trips.ofCurrent.id
    });
  });

  it("update Trip startDate after endDate", async () => {
    await updateTrip(defaults.contexts.user, ERROR_MESSAGE.START_DATE_AFTER_END, {
      startDate: faker.date.future(),
      endDate: faker.date.recent(),
      id: defaults.trips.ofCurrent.id
    });
  });

  it("update Trip just comment", async () => {
    await updateTrip(defaults.contexts.user, null, {
      comment: faker.lorem.sentence(),
      id: defaults.trips.ofCurrent.id
    });
  });

  it("update Trip just destination", async () => {
    await updateTrip(defaults.contexts.user, null, {
      destination: faker.address.city(),
      id: defaults.trips.ofCurrent.id
    });
  });
});