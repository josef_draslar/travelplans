# Test app - Travel Plans

Canary test environment: https://josef-travelplans.herokuapp.com/

### Users

|  role   | email | password |
| ------  | ------ | ------ |
|  user   | user@test.com | 123456 |
| manager | user_manager@test.com | 123456 |
|  admin  | admin@test.com | 123456 |


# To run locally

  - install node, yarn, typescript, psql, postgres server
  - set up database
  - create .env file in /backend dir according to .env.example
  - to run in dev mode call (in root dir):
  ```sh
$ yarn dev
```

# To run backend tests locally

  - set up test database according to .env file in /backend dir
  - to run tests call (in root of backend - /backend): 
 ```sh
$ yarn test
```

# To run frontend e2e tests locally

  - run the server locally (see above)
  - in another (terminal) window run (in /frontend) 
 ```sh
$ yarn wdio
```

# Next dev steps
  - fix backend express-graphql to return correct error codes (currently returns 500 even if it should return e.g. 400)
  - frontend hide menu items for log out user that are accessible only while logged in