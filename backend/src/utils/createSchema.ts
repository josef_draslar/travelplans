import {buildSchema} from "type-graphql";
import {Container} from "typedi";
import {customAuthChecker} from "../auth/custom-auth-checker";
import {TripResolver} from "../resolver/TripResolver";
import {UserResolver} from "../resolver/UserResolver";

export const createSchema = () => buildSchema({
        resolvers: [TripResolver, UserResolver],
        container: Container,
        authChecker: customAuthChecker,
    });