import Document, { Html, Main, NextScript } from "next/document";
import Head from "../components/Head";

export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
