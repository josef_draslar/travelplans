import { Context } from "../../src/auth/Context.interface";
import { Role, User } from "../../src/entity/User";
import { Trip } from "../../src/entity/Trip";
import { getManager, Repository } from "typeorm";
import * as faker from "faker";

interface TestTrips {
  ofCurrent: Trip;
  ofDifferent: Trip;
  count: number;
}

interface TestUsers {
  current: User;
  different: User;
  count: number;
}

interface TestRepositories {
  user: Repository<User>;
  trip: Repository<Trip>;
}

interface TestContexts {
  user: Context;
  manager: Context;
  admin: Context;
}

export interface TestDataDefaults {
  contexts: TestContexts;
  users: TestUsers;
  trips: TestTrips;
  repos: TestRepositories;
}

export const initTestData = async () => {
  const repos: TestRepositories = {
    user: getManager().getRepository(User),
    trip: getManager().getRepository(Trip)
  };

  await repos.trip.delete({});
  await repos.user.delete({});

  const users: TestUsers = {
    current: await createDbUser(repos.user),
    different: await createDbUser(repos.user),
    count: 2
  };

  const trips: TestTrips = {
    ofCurrent: await createDbTrip(repos.trip, users.current),
    ofDifferent: await createDbTrip(repos.trip, users.different),
    count: 2
  };

  const contexts: TestContexts = {
    user: createContext(users.current, Role.user),
    manager: createContext(users.current, Role.user_manager),
    admin: createContext(users.current, Role.admin)
  };

  const result: TestDataDefaults = {
    contexts,
    users,
    trips,
    repos
  };

  return result;
};

const createContext = (user: User, role: Role): Context => ({
  user: { ...user, role }
});

const createDbUser = (repo: Repository<User>): Promise<User> =>
  repo.save(
    repo.create({
      email: faker.internet.email(),
      password: faker.internet.password(6, null, "Aa")
    })
  );

const createDbTrip = (repo: Repository<Trip>, user: User): Promise<Trip> =>
  repo.save(
    repo.create({
      destination: faker.address.city(),
      comment: faker.lorem.sentence(),
      startDate: faker.date.recent(),
      endDate: faker.date.future(),
      user
    })
  );
