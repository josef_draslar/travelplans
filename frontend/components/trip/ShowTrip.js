import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { pushNotificationMessageIfPossible } from "../NotificationMessage";
import { PageTitle } from "../BasicElements";
import moment from "moment";
import Button from "../Button";
import { deleteTrip } from "../../redux/actions/UserActions";
import { getError, getIsLoading, initLoading } from "../../utils/Utils";
import { Alert } from "reactstrap";
import colors from "../../utils/colors";
import { enrichTrip } from "./TripsList";

const ShowTrip = ({ t, trip, setEditScreen, router }) => {
  const [isLoading, setLoading] = useState(false);
  const dispatch = useDispatch();

  if (!trip) return null;

  const onComplete = p => {
    if (p === false) {
      router.push("/");
      pushNotificationMessageIfPossible({
        title: t("Trip.removed"),
        type: "SUCCESS"
      });
    }
    setLoading(p);
  };

  const remove = e => dispatch(deleteTrip(trip.id, initLoading(onComplete)));
  const error = getError(isLoading);
  const { destination, startDate, endDate, comment, startsIn } = enrichTrip(
    trip
  );
  return (
    <div>
      <Content>
        <PageTitle>
          {destination}
        </PageTitle>
        {startsIn > 0 &&
          <p>
            {startsIn.toFixed(0) + " " + t("Trip.toStart")}
          </p>}
        <p>
          {moment(startDate).format("M/D/YY") +
            " - " +
            moment(endDate).format("M/D/YY")}
        </p>
        <p>
          {comment}
        </p>
      </Content>
        {error &&
        <Alert color="danger">
            {error}
        </Alert>}
      <Button onClick={() => setEditScreen(true)} iconKey={"pen"} isButton>
        {t("Base.edit")}
      </Button>

      <Span>
        <Button
          onClick={remove}
          iconKey={"trash"}
          isLoading={getIsLoading(isLoading)}
          backgroundColor={colors.error}
          hoverColor={colors.errorHover}
          isButton
        >
          {t("Base.delete")}
        </Button>
      </Span>
    </div>
  );
};

ShowTrip.propTypes = {
  trip: PropTypes.object.isRequired,
  setEditScreen: PropTypes.func.isRequired,
  router: PropTypes.object.isRequired,
  t: PropTypes.object.isRequired
};

export default ShowTrip;

const Span = styled.span`margin-left: 20px;`;

const Content = styled.div`margin-bottom: 50px;`;
