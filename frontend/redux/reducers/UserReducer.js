export const UPDATE_STORE = "UPDATE_STORE",
  ADD_TRIP = "ADD_TRIP",
  UPDATE_TRIP = "UPDATE_TRIP",
  DELETE_TRIP = "DELETE_TRIP",
    DELETE_USER = "DELETE_USER",
    UPDATE_USER = "UPDATE_USER";
export const SIGN_IN = "SIGN_IN",
  SIGN_UP = "SIGN_UP",
  RESET_PASSWORD = "RESET_PASSWORD",
  CHANGE_PASSWORD = "CHANGE_PASSWORD";

const safelyParseJSON = (json, badValue = {}) => {
    let parsed = badValue;

    try {
        parsed = JSON.parse(json);
    } catch (e) {}

    return parsed;
};

const initialState = {
  token: process.browser && window ? safelyParseJSON(window.localStorage.getItem("token"),null) : null,
  user: process.browser && window ? safelyParseJSON(window.localStorage.getItem("user"),null) : null,
  trips: null,

  users: null,
  allTrips: null
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case UPDATE_STORE:
      return {
        ...state,
        ...action.payload
      };

    case ADD_TRIP:
      return {
        ...state,
        trips: [...(state.trips || []), action.payload]
      };
    case UPDATE_TRIP:
      return {
        ...state,
        trips: (state.trips || []).map(
          trip => (trip.id === action.payload.id ? action.payload : trip)
        )
      };
    case DELETE_TRIP:
      return {
        ...state,
        trips: (state.trips || []).filter(({ id }) => id !== action.payload)
      };
      case UPDATE_USER:
          return {
              ...state,
              users: (state.users || []).map(
                  user => (user.id === action.payload.id ? action.payload : user)
              )
          };
    case DELETE_USER:
      return {
        ...state,
        users: (state.users || []).filter(({ id }) => id !== action.payload)
      };
    default:
      return state;
  }
};

export default user;
