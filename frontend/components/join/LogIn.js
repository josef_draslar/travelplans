import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Alert, Form, FormGroup, Input } from "reactstrap";
import Button from "../Button";
import {
    ButtonWrapper,
    H6,
    LabelLocal,
    Or,
    Text,
    EmailWrapper,
    FormWrapper, A
} from "./ModalJoinElements";
import { withTranslation } from "../../utils/multiLanguageSupport";
import { FacebookButton } from "./FacebookButton";
import { useDispatch } from "react-redux";
import {getOnChange, initLoading} from "../../utils/Utils";
import { ModalHeader } from "../navbar/ModalHeader";
import { fetchAndLogInUser } from "../../redux/actions/UserActions";
import NotificationMessage, {
    removeNotificationMessageInnerCallback,
    setNotificationMessageInnerCallback
} from "../NotificationMessage";

const LogInForm = ({ t }) => {
  const [form, setForm] = useState({
    user: "",
    password: ""
  });
  const { email, password } = form;
  const [isLoading, setLoading] = useState(false);
  const dispatch = useDispatch();

  const onChange = getOnChange(setForm, form, setLoading, isLoading);

  const onSubmit = e => {
    e.preventDefault();

    dispatch(
      fetchAndLogInUser(
        {
          email,
          password
        },
        initLoading(setLoading)
      )
    );
  };

  const isDisabled = () => !(email && password && password.length > 5);

  return (
    <Form onSubmit={onSubmit}>
      <FormGroup>
        <LabelLocal htmlFor="email-field">
          {t("Join.email").toUpperCase()}
        </LabelLocal>
        <Input
          id="email-field"
          type="text"
          name="email"
          value={email}
          onChange={onChange}
        />
      </FormGroup>
      <FormGroup>
        <LabelLocal htmlFor="password-field">
          {t("Join.password").toUpperCase()}
        </LabelLocal>
        <Input
          id="password-field"
          type="password"
          name="password"
          value={password}
          onChange={onChange}
        />
      </FormGroup>
      {typeof isLoading === "string" &&
        <Alert color="danger">
          {isLoading}
        </Alert>}
      <ButtonWrapper>
        <Button
          disabled={isDisabled()}
          isLoading={isLoading === true}
          type="submit"
          isButton
        >
          {t("NavBar.signIn").toUpperCase()}
        </Button>
      </ButtonWrapper>
    </Form>
  );
};

LogInForm.propTypes = {
  t: PropTypes.object.isRequired
};

const LogIn = ({ t, swap, openPasswordReset, allowFacebook }) => {
    const [notificationMessage, setNotificationMessge] = useState(null);


    const displayNotificationMessage = message => {
        setNotificationMessge(<NotificationMessage messageObject={message} t={t} />);
        setTimeout(() => setNotificationMessge(null), 8000);
    };

    useEffect(
        ()=>{
            setNotificationMessageInnerCallback("log-in",displayNotificationMessage);
            return ()=>removeNotificationMessageInnerCallback("log-in");
        },
        [],
    );

    return (
  <div>
    <ModalHeader>
      {t("NavBar.signIn")}
    </ModalHeader>
      {notificationMessage && notificationMessage}
    {allowFacebook
      ? <div className="row">
          <div className="col-sm-6">
            <H6>
              {t("Join.withSocialMedia")}
            </H6>
            <FacebookButton />
          </div>
          <div className="col-sm-6">
            <EmailWrapper>
              <Or>
                {t("Base.or")}
              </Or>
              <H6>
                {t("Join.withEmail")}
              </H6>
              <LogInForm t={t} />
            </EmailWrapper>
          </div>
        </div>
      : <FormWrapper>
          <LogInForm t={t} />
        </FormWrapper>}

    <Text>
      {t("Join.noAccount")}{" "}
      <A onClick={swap}>{t("NavBar.signUp")}</A>
    </Text>
    {/*<Text>
      <AOnClick onClick={openPasswordReset}>
        {t("Join.resetPassword")}
      </AOnClick>
    </Text>*/}
  </div>
    )};

LogIn.propTypes = {
  swap: PropTypes.func.isRequired,
  openPasswordReset: PropTypes.func.isRequired,
  allowFacebook: PropTypes.bool
};

export default withTranslation(LogIn);
