import React from "react";
import styled from "styled-components";
import colors from "../utils/colors";
import { Container, Col, Row } from "reactstrap";
import fonts from "../utils/fonts";

export default () => (
    <Wrapper>
      <Container>
        <Row>
          <Col style={{ marginTop: "20px" }}>
            <span style={{ color: "rgb(100,100,100)" }}>
              2020 © travelplans
            </span>
          </Col>
        </Row>
      </Container>
    </Wrapper>
  );

const Wrapper = styled.div`
  margin-top: 70px;
  border-top: rgb(50, 50, 50) solid 1px;
  padding-top: 25px;
  text-align: center;
  background-color: ${colors.white};
  font-family: ${fonts.text};
`;