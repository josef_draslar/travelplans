import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { media } from "../../utils/Media";
import { useDispatch, useSelector } from "react-redux";
import { fireLocalAction } from "../../redux/actions/LocalActions";
import { localActions } from "../../redux/reducers/LocalReducer";
import { NavigationItem } from "./MenuStyledComponents";
import {logOutUser} from "../../redux/actions/UserActions";

const NavMain = ({ navBarWhite }) => {
  const dispatch = useDispatch();
  const openSignIn = () => dispatch(fireLocalAction(localActions.OPEN_SIGN_IN));
  const openSignUp = () => dispatch(fireLocalAction(localActions.OPEN_SIGN_UP));
  const logOut = () => dispatch(logOutUser());
  const user = useSelector(state => state.user.user);


  return (
    <Ul>
      <NavigationItem
        url="/"
        name="main"
        hideForMobile
        navBarWhite={navBarWhite}
      />
      <NavigationItem
        url="/trip/itinerary"
        name="itinerary_page"
        hideForMobile
        navBarWhite={navBarWhite}
      />
      {!user && <NavigationItem
            onClick={openSignIn}
            name="signIn"
            hideForMobile
            navBarWhite={navBarWhite}
          />}
      {user
        ? <NavigationItem
              onClick={logOut}
              name="logOut"
              hideForMobile
              navBarWhite={navBarWhite}
          />
        : <NavigationItem
            onClick={openSignUp}
            name="signUp"
            hideForMobile
            navBarWhite={navBarWhite}
          />}
    </Ul>
  );
};

NavMain.propTypes = {
  userToken: PropTypes.object,
  navBarWhite: PropTypes.bool
};

export default NavMain;

const Ul = styled.ul`
  display: inline-block;
  float: right;
  & li {
    display: inline-block;
  }
  margin-bottom: 0;
  margin-top: 0;
  height: 100%;

  ${media.lessThan("sm")`
    padding-left: 0;
  `};
`;
