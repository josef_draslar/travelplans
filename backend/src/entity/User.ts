import {Entity, PrimaryGeneratedColumn, Column, OneToMany} from "typeorm";
import { InputType, Field, ObjectType, ID } from "type-graphql";
import {Trip} from "./Trip";

export enum Role {
    admin,
    user_manager,
    user
}

@Entity()
export class User {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    email: string;

    @Column()
    password: string;

    @Column({"default": new Date()})
    createdAt: Date;

    @Column({"default": Role.user})
    role: Role;

    @OneToMany(type => Trip, trip => trip.user, { onDelete: 'CASCADE' })
    trips: Trip[];

    constructor(email: string, password: string, role: Role = Role.user, createdAt: Date = new Date()){
        this.email = email;
        this.password = password;
        this.role = role;
        this.createdAt = createdAt;
    }

    static createMultiple(userArray: Array<{email: string, password: string, role?: Role, createdAt?: Date}>): Array<User>{
        return userArray.map(({email, password, role, createdAt})=>new User(email,password,role,createdAt))
    }
}

@ObjectType()
export class UserRead {

    @Field(() => Number)
    id: number;

    @Field(() => String)
    email: string;

    @Field(() => String)
    role?: Role;

    @Field(() => [Trip],{nullable: true})
    trips?: Trip[];
}

@InputType()
export class UserCreate {

    @Field(() => String)
    email: string;

    @Field(() => String)
    password: string;
}

@InputType()
export class UserUpdate {
    @Field({nullable: true})
    id?: number;

    @Field({nullable: true})
    email?: string;

    @Field({nullable: true})
    password?: string;
}

@ObjectType()
export class UserWithToken {
    @Field(() => UserRead)
    user: UserRead;

    @Field(() => String)
    token: string;
}