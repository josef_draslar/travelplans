const logInUser = `
  mutation LogInUser($data: UserCreate!){
      logInUser(data: $data){
        user {
          id,
          email,
          role,
          trips {
              id,
              destination,
              startDate,
              endDate,
              comment,
          }
        },
        token
      }
    }
`;

const createUser = `
  mutation CreateUser($data: UserCreate!){
      createUser(data: $data){
        user {
          id,
          email
        },
        token
      }
    }
`;

const updateUser = `
  mutation UpdateUser($data: UserUpdate!){
      updateUser(data: $data){
          id,
          email
      }
    }
`;

const changeRoleUser = `
  mutation ChangeRoleUser($id: Float!){
      changeRoleUser(id: $id){
          id,
          email,
          role
      }
    }
`;

const resetUserPassword = `
  mutation ResetUserPassword($id: Float!){
      resetUserPassword(id: $id)
    }
`;

const deleteUser = `
  mutation DeleteUser($id: Float!){
      deleteUser(id: $id)
    }
`;

export const UserMutations = {
    logInUser,
    createUser,
    updateUser,
    deleteUser,
    resetUserPassword,
    changeRoleUser
};