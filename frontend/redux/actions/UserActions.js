import { getGraphqlClient } from "../../utils/configureGraphqlClient";
import {
  userMutations,
  userQueries,
  tripMutations,
  tripQueries
} from "../../utils/constantsHelper";
import * as actions from "../reducers/UserReducer";
import { fireLocalAction } from "./LocalActions";
import { localActions } from "../reducers/LocalReducer";

const getTripUtc = trip => ({
  ...trip,
  startDate: trip.startDate
    ? new Date(trip.startDate).toUTCString()
    : trip.startDate,
  endDate: trip.endDate ? new Date(trip.endDate).toUTCString() : trip.endDate
});

// functions are intentionally not pure as they depend on external (redux store) values (mostly user object), to simplify usage of these actions

export const fetchAndLogInUser = (data, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient()
        .request(userMutations.logInUser, { data })
        .then(response => {
          const accessTokenStringified = JSON.stringify(
            response.logInUser.token
          );
          window.localStorage.setItem("token", accessTokenStringified);
          window.localStorage.setItem(
            "user",
            JSON.stringify({ id: response.logInUser.user.id })
          );

          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              token: response.logInUser.token,
              user: {
                ...response.logInUser.user,
                trips: undefined
              },
              trips: response.logInUser.user.trips
            }
          });

          getState().local.joinModal &&
            fireLocalAction(localActions.CLOSE_JOIN)(dispatch);
        }),
      dispatch
    );
  };
};

export const createUser = (data, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient()
        .request(userMutations.createUser, { data })
        .then(response => {
          const accessTokenStringified = JSON.stringify(
            response.createUser.token
          );
          window.localStorage.setItem("token", accessTokenStringified);
          window.localStorage.setItem(
            "user",
            JSON.stringify({ id: response.createUser.user.id })
          );

          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              token: response.createUser.token,
              user: {
                ...response.createUser.user,
                trips: undefined
              },
              trips: response.createUser.user.trips
            }
          });

          getState().local.joinModal &&
            fireLocalAction(localActions.CLOSE_JOIN)(dispatch);
        }),
      dispatch
    );
  };
};

export const updateUser = (data, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userMutations.updateUser, { data })
        .then(response => {
          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              user: response.updateUser
            }
          });
        }),
      dispatch
    );
  };
};

export const logOutUser = cb => {
  return dispatch => {
    window.localStorage.removeItem("token");
    window.localStorage.removeItem("user");

    dispatch({
      type: actions.UPDATE_STORE,
      payload: {
        user: null,
        token: null,
        trips: null,

        users: null,
        allTrips: null
      }
    });
    cb && cb();
  };
};

export const deleteUser = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userMutations.deleteUser, { id })
        .then(() => {
          if (id === getState().user.user.id) logOutUser()(dispatch);
          else
            dispatch({
              type: actions.DELETE_USER,
              payload: id
            });
        }),
      dispatch
    );
  };
};

export const changeRoleUser = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userMutations.changeRoleUser, { id })
        .then(response => {
          dispatch({
            type: actions.UPDATE_USER,
            payload: response.changeRoleUser
          });
        }),
      dispatch
    );
  };
};

export const resetUserPassword = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userMutations.resetUserPassword, { id })
        .then(() => {}),
      dispatch
    );
  };
};

export const getUser = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userQueries.getUser, { id })
        .then(response => {
          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              user: {
                ...response.getUser,
                trips: undefined
              },
              trips: response.getUser.trips
            }
          });
        }),
      dispatch
    );
  };
};

export const refetchUser = () => {
  return (dispatch, getState) => {
    const user = getState().user.user;
    const token = getState().user.token;
    if (!token || !user || !user.id || user.email) return;

    const logOutAfterFetchFails = promise => {
      console.log("log out after fetch fails");
      promise.catch(() => logOutUser()(dispatch));
    };

    getUser(user.id, logOutAfterFetchFails)(dispatch, getState);
  };
};

//TRIP
export const createTrip = (data, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(tripMutations.createTrip, { data: getTripUtc(data) })
        .then(response => {
          dispatch({
            type: actions.ADD_TRIP,
            payload: response.createTrip
          });
        }),
      dispatch
    );
  };
};

export const updateTrip = (data, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(tripMutations.updateTrip, { data: getTripUtc(data) })
        .then(response => {
          dispatch({
            type: actions.UPDATE_TRIP,
            payload: response.updateTrip
          });
        }),
      dispatch
    );
  };
};

export const deleteTrip = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(tripMutations.deleteTrip, { id })
        .then(() => {
          dispatch({
            type: actions.DELETE_TRIP,
            payload: id
          });
        }),
      dispatch
    );
  };
};

// ADMIN
export const getUsers = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(userQueries.getUsers)
        .then(response => {
          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              users: response.getUsers
            }
          });
        }),
      dispatch
    );
  };
};
export const getTrips = (id, getPromise = () => {}) => {
  return (dispatch, getState) => {
    getPromise(
      getGraphqlClient(getState().user.token)
        .request(tripQueries.getTrips)
        .then(response => {
          dispatch({
            type: actions.UPDATE_STORE,
            payload: {
              allTrips: response.getTrips
            }
          });
        }),
      dispatch
    );
  };
};
