import { getRepository } from "typeorm";

import { Role, User } from "../entity/User";
import { Trip } from "../entity/Trip";
import bcrypt from "bcrypt";

export async function seedDatabase() {
  const tripRepository = getRepository(Trip);
  const userRepository = getRepository(User);

  const userCount = await userRepository.count();

  if (userCount > 0) return;

  const password = bcrypt.hashSync("123456", 10);

  const [admin, userManager, user] = User.createMultiple([
    {
      email: "admin@test.com",
      password,
      role: Role.admin
    },
    {
      email: "user_manager@test.com",
      password,
      role: Role.user_manager
    },
    {
      email: "user@test.com",
      password,
      role: Role.user
    }
  ]);
  await userRepository.save([admin, userManager, user]);

  const today = new Date();
  const tomorrow = new Date();
  tomorrow.setDate(today.getDay()+1);
  const yesterday = new Date();
  yesterday.setDate(today.getDay()-1);
  const future = new Date(2030, 1, 10);
  const nearFuture = new Date();
  nearFuture.setDate(today.getDay()+30);

  const trips = Trip.createMultiple([
    {
      destination: "Amsterdam",
      comment: "trip",
      startDate: new Date(2030, 1, 10),
      endDate: new Date(2030, 1, 20),
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: new Date(2030, 1, 20),
      endDate: new Date(2030, 2, 25),
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: today,
      endDate: today,
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: yesterday,
      endDate: yesterday,
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: yesterday,
      endDate: today,
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: yesterday,
      endDate: future,
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: today,
      endDate: nearFuture,
      user: user
    },
    {
      destination: "London",
      comment: "trip",
      startDate: nearFuture,
      endDate: future,
      user: user
    }
  ]);
  await tripRepository.save(trips);
}

export type Lazy<T extends object> = Promise<T> | T;
