import {Connection, createConnection} from "typeorm";

let connection: Connection;

export const testConnection = async (drop: boolean = false) => {
    if(!connection) {
        connection = await createConnection({
            type: "postgres",
            url: process.env.DATABASE_URL_TEST,
            synchronize: drop,
            dropSchema: drop,
            logging: false,
            entities: [
                __dirname + "/../../src/entity/*.ts"
            ],
            subscribers: [
                __dirname + "/../../src/subscriber/*.ts"
            ],
            migrations: [
                __dirname + "/../../src/migration/*.ts"
            ],
        });
    }
    return connection;
};

export const closeConnection = async () =>
    this.connection && (await this.connection.close());