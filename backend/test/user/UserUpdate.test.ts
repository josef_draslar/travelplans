import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import faker from "faker";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import {
  User,
  UserUpdate
} from "../../src/entity/User";
import { UserMutations } from "../../src/constants-shared/UserMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const updateUser = async (
  context?: Context,
  expectedErrorMessage?: string,
  params: object = {},
  userP: User = defaults.users.current
) => {
  const userToUpdate: UserUpdate = {
    id: userP.id,
    email: faker.internet.email(),
    password: faker.internet.password(6, false, null, "aA"),
    ...params
  };
  const response: ExecutionResult = await getGraphQL(
    {
      source: UserMutations.updateUser,
      variableValues: {
        data: userToUpdate
      }
    },
    context
  );

  const user: UserUpdate = {
    id: userP.id,
    email: userToUpdate.email ? userToUpdate.email : userP.email
  };

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);

    const userDb: User = await defaults.repos.user.findOne({
      where: { id: userP.id }
    });

    expect(userDb).toMatchObject(userP);
  } else {
    expect(response.errors).not.toBeDefined();
    expect(response).toMatchObject({
      data: {
        updateUser: {
          id: user.id,
          email: user.email
        }
      }
    });

    const userDb: User = await defaults.repos.user.findOne({
      where: { id: userP.id }
    });

    expect(userDb).toMatchObject(user);
  }
};

describe("User Update", () => {
  it("update User no auth", async () => {
    await updateUser(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("update User as user", async () => {
    await updateUser(defaults.contexts.user);
  });

  it("update User as manager", async () => {
    await updateUser(defaults.contexts.manager);
  });

  it("update User as admin", async () => {
    await updateUser(defaults.contexts.admin);
  });

  it("update User different as user", async () => {
    await updateUser(
      defaults.contexts.user,
      ERROR_MESSAGE.ACCESS_DENIED,
      undefined,
      defaults.users.different
    );
  });

  it("update User different as manager", async () => {
    await updateUser(
      defaults.contexts.manager,
      null,
      undefined,
      defaults.users.different
    );
  });

  it("update User different as admin", async () => {
    await updateUser(
      defaults.contexts.admin,
      null,
      undefined,
      defaults.users.different
    );
  });

  it("update User just email", async () => {
    await updateUser(defaults.contexts.user, null, { password: undefined });
  });

  it("update User just password", async () => {
    await updateUser(defaults.contexts.user, null, { email: undefined });
  });

  it("update User non existing", async () => {
    await updateUser(defaults.contexts.admin, ERROR_MESSAGE.NOT_FOUND, {
      id: defaults.users.different.id + 100
    });
  });
});