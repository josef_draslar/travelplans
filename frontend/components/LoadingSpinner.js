import React from "react";
import styled from "styled-components";

const LoadingSpinnerInner = styled.i.attrs(props => ({
    className: "fas fa-spinner fa-spin fa-3x"
}))``;

const LoadingSpinnerWrapper = styled.div`
  margin-top: 20px;
  margin-bottom: 20px;
  text-align: center;
`;

export default () => (
    <LoadingSpinnerWrapper>
        <LoadingSpinnerInner />
    </LoadingSpinnerWrapper>
);