import {info} from "./controller/GeneralController";

/**
 * All application routes.
 */
export const AppRoutes = [
    {
        path: "/info",
        method: "get",
        action: info
    }
];