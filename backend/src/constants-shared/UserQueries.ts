const getUser = `
  query GetUser($id: Float!){
      getUser(id: $id){
          id,
          email,
          role,
          trips {
              id,
              destination,
              startDate,
              endDate,
              comment,
          }
      }
    }
`;

const getUsers = `
  query GetUsers {
      getUsers {
          id,
          email,
          role,
      }
    }
`;

export const UserQueries = {
    getUser,
    getUsers
};