let lastId = 1000;

export const getUniqueId = (prefix='id') => {
    lastId++;
    return `${prefix}${lastId}`;
};
