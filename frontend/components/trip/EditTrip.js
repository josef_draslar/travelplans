import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useDispatch } from "react-redux";
import { pushNotificationMessageIfPossible } from "../NotificationMessage";
import moment from "moment";
import Button from "../Button";
import {createTrip, updateTrip} from "../../redux/actions/UserActions";
import {
  getError,
  getIsLoading,
  getOnChange,
  initLoading
} from "../../utils/Utils";
import { Alert } from "reactstrap";
import Input from "../Input";
import colors from "../../utils/colors";

const EditTrip = ({ t, trip, setEditScreen, router }) => {
  const [form, setForm] = useState({ ...trip });
  const [isLoading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const isCreate = !setEditScreen;

  useEffect(
    () => {
      setForm(trip);
    },
    [trip]
  );

  const onComplete = p => {
    if (p === false) {
      isCreate ? router.push("/") : setEditScreen(false);
      pushNotificationMessageIfPossible({
        title: t(isCreate?"Trip.created":"Trip.updated"),
        type: "SUCCESS"
      });
    }
    setLoading(p);
  };

  const onChange = getOnChange(setForm, form, setLoading, isLoading);
  const submit = isCreate?createTrip:updateTrip;
  const update = e => dispatch(submit(form, initLoading(onComplete)));
  const isEnabled = () => form.destination && form.startDate && form.endDate;
  const error = getError(isLoading);

  return (
    <div>
      <Content>
        <Input
          name="destination"
          onChange={onChange}
          value={form.destination}
          t={t}
        />
        <Input
          name="startDate"
          onChange={onChange}
          value={moment(form.startDate).format("YYYY-MM-DD")}
          max={moment(form.endDate).format("YYYY-MM-DD")}
          type="date"
          t={t}
        />
        <Input
          name="endDate"
          onChange={onChange}
          value={moment(form.endDate).format("YYYY-MM-DD")}
          min={moment(form.startDate).format("YYYY-MM-DD")}
          type="date"
          t={t}
        />
        <Input
          name="comment"
          onChange={onChange}
          value={form.comment}
          type="textarea"
          t={t}
        />
      </Content>
      {error &&
        <Alert color="danger">
          {error}
        </Alert>}
      {isCreate ?
          <Button
              href="/"
              inSiteLink
              iconKey={"times"}
              backgroundColor={colors.lightGray}
              hoverColor={colors.lighterGray}
          >
              {t("Base.back")}
          </Button>
          :
        <Button
          onClick={() => setEditScreen(false)}
          iconKey={"times"}
          backgroundColor={colors.lightGray}
          hoverColor={colors.lighterGray}
          isButton
        >
          {t("Base.cancel")}
        </Button>}
      <Span>
        <Button
          onClick={update}
          disabled={!isEnabled()}
          isLoading={getIsLoading(isLoading)}
          isButton
        >
          {t("Base.save")}
        </Button>
      </Span>
    </div>
  );
};

EditTrip.propTypes = {
  trip: PropTypes.object.isRequired,
  t: PropTypes.object.isRequired,
  router: PropTypes.object,
  setEditScreen: PropTypes.func
};

export default EditTrip;

const Span = styled.span`margin-left: 20px;`;

const Content = styled.div`margin-bottom: 50px;`;
