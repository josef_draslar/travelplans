import React from "react";
import RoutesWrapper from "../../utils/RoutesWrapper";
import EditTrip from "../../components/trip/EditTrip";
import { withTranslation } from "../../utils/multiLanguageSupport";
import { PageTitle } from "../../components/BasicElements";
import { useRouter } from "next/router";
import MustSignInPage from "../../components/trip/MustSignIn";
import {useSelector} from "react-redux";

const CreateTripPage = ({t}) => {
  const user = useSelector(state => state.user.user);
  const router = useRouter();
  const now = new Date();

  if(!user || !user.email) return <MustSignInPage />;

  return (
    <RoutesWrapper showHeaderShadow>
        <PageTitle>
          {t("NavBar.create_page")}
        </PageTitle>
        <EditTrip
          trip={{
            destination: "",
            comment: "",
            startDate: now,
            endDate: now,
            userId: user.id
          }}
          t={t}
          router={router}
        />
    </RoutesWrapper>
  );
};

export default withTranslation(CreateTripPage);
