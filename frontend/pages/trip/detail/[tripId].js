import React, {useState} from "react";
import RoutesWrapper from "../../../utils/RoutesWrapper";
import { useRouter } from 'next/router';
import {useSelector} from "react-redux";
import {pushNotificationMessageIfPossible} from "../../../components/NotificationMessage";
import {withTranslation} from "../../../utils/multiLanguageSupport";
import ShowTrip from "../../../components/trip/ShowTrip";
import EditTrip from "../../../components/trip/EditTrip";
import MustSignInPage from "../../../components/trip/MustSignIn";

const DetailTrip = ({t}) => {
    const [isEditScreen, setEditScreen] = useState(false);
    const router = useRouter();
    const { tripId } = router.query;
    const trips = useSelector(state => state.user.trips);

    if(!trips)  return <MustSignInPage />;

    const tripsFiltered = trips.filter(({id})=>""+id===tripId);

    if(tripsFiltered.length === 0) {
        router.push("/");
        pushNotificationMessageIfPossible({
            title: t("Trip.tripNotFound"),
            type: "SUCCESS"
        });
    }

    const trip = tripsFiltered[0];

        return (
            <RoutesWrapper
                showHeaderShadow
            >
                {isEditScreen?
                <EditTrip trip={trip} setEditScreen={setEditScreen} t={t} />
                    :
                    <ShowTrip trip={trip} setEditScreen={setEditScreen} router={router} t={t} />}
            </RoutesWrapper>
        );
};

export default withTranslation(DetailTrip);