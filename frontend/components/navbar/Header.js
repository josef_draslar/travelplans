import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import colors from "../../utils/colors";
import fonts from "../../utils/fonts";
import { media } from "../../utils/Media";
import Button from "../../components/Button";

class Header extends React.PureComponent {
  render() {
    const {
      text,
      src,
      darken,
      smaller,
      linkButton,
      smallest,
      center,
      notFixed,
      smallerText,
        position
    } = this.props;

    return (
      <HeaderStyled
        src={src}
        smaller={!!smaller}
        smallest={!!smallest}
        center={!!center}
        notFixed={!!notFixed}
        darken={darken}
        position={position}
      >
        <H2>
          {text}
          {smallerText
            ? <H3>
                {smallerText}
              </H3>
            : ""}
        </H2>
        {linkButton &&
          <ButtonWrapper>
            <Button href={linkButton.to} inSiteLink>
              {linkButton.text}
            </Button>
          </ButtonWrapper>}
      </HeaderStyled>
    );
  }
}

Header.propTypes = {
  text: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  smallerText: PropTypes.string,
    position: PropTypes.string,
  linkButton: PropTypes.object,
  darken: PropTypes.number,
  smaller: PropTypes.bool,
  smallest: PropTypes.bool,
  notFixed: PropTypes.bool,
  center: PropTypes.bool
};

export default Header;

const H2 = styled.h2`
  position: absolute;
  top: 50%;
  margin-top: -32px;
  margin-left: 40px;
  color: ${colors.white};
  font-size: 65px;
  line-height: 65px;
  font-family: ${fonts.mainTitle};
  ${media.lessThan("sm")`
    line-height: 45px;
    font-size: 45px;
    padding-left: 10px;
    padding-right: 10px;
    margin-left: 0;
  `};
`;

const H3 = styled.h3`
  display: block;
  font-family: ${fonts.mainTitle};
  font-size: 35px;
  padding-left: 20px;
`;

const HeaderStyled = styled.div`
  margin-top: -106px;
  z-index: 100;
  background-color: #fff;
  position: relative;

  height: ${props => (props.smallest ? 350 : props.smaller ? 500 : 666)}px;
  background-color: rgb(156, 156, 156);
  
    background-image: url('${props => props.src}');
    background-position: ${props => (props.center ? "center" : props.position ? props.position : "center 0px")};
  background-size: cover;
  ${props => !props.notFixed && "background-attachment: fixed;"}

  ${media.lessThan("sm")`
      margin-top: 0;
      height: unset;
      padding-top: 56%;
      background-attachment: scroll;
    `};
    
    &:before {
      content: " ";
      position: absolute;
      top: 0;
      left: 0;
      bottom: 0;
      right: 0;
      background: black;
      opacity: ${props => (props.darken ? props.darken : 0.2)};
    }
`;

const ButtonWrapper = styled.div`
  position: absolute;
  bottom: 40px;
  right: 0;
`;
