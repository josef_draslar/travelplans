import React, { useEffect } from "react";
import PropTypes from "prop-types";
import NextNProgress from "nextjs-progressbar";
import styled from "styled-components";

import NavBar from "../components/navbar/NavBar";
import Footer from "../components/Footer";
import IphoneAddToHp from "../components/IphoneAddToHp";
import bugsnagClient from "../utils/BugsnagClient";
import { withTranslation } from "./multiLanguageSupport";
import { useRouter } from "next/router";

class FallBackScreen extends React.PureComponent {
  render() {
    window.location.href = "/";
    return null;
  }
}

//TODO add bugsnag beforesend callback
const ErrorBoundary = bugsnagClient.getPlugin("react");

const RoutesWrapper = ({
  hideNavBar,
  navBarWhite,
  showHeaderShadow,
  showBorder,
  hideFooter,
  children,
  t
}) => {
  const router = useRouter();
  useEffect(() => {document && (document.title = t("Title." + router.pathname))}, []);
  return (
    <ErrorBoundary fallbackComponent={FallBackScreen}>
      <div style={{ height: "100%" }}>
        <IphoneAddToHp />
        {!hideNavBar &&
          <NavBar
            navBarWhite={navBarWhite}
            showHeaderShadow={showHeaderShadow}
            showBorder={showBorder}
          />}
        <NextNProgress />
        <Wrapper>
          <div>
            {children}
          </div>
          {!hideFooter && <Footer />}
        </Wrapper>
      </div>
    </ErrorBoundary>
  );
};

RoutesWrapper.propTypes = {
  title: PropTypes.string,
  hideNavBar: PropTypes.bool,
  navBarWhite: PropTypes.bool,
  showHeaderShadow: PropTypes.bool,
  showBorder: PropTypes.bool
};

RoutesWrapper.defaultProps = {
  hideNavBar: false
};

export default withTranslation(RoutesWrapper);

const Wrapper = styled.div.attrs(props => ({
  className: "container"
}))`
  margin-top: 50px;
`;
