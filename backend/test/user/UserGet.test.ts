import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import {
  User,
} from "../../src/entity/User";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
import { UserQueries } from "../../src/constants-shared/UserQueries";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const getUser = async (
  context?: Context,
  expectedErrorMessage?: string,
  id: number = defaults.users.current.id
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: UserQueries.getUser,
      variableValues: {
        id
      }
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
  } else {
    expect(response.errors).not.toBeDefined();

    const userDb: User = await defaults.repos.user.findOne({
      where: { id },
      relations: ["trips"]
    });

    expect(response).toMatchObject({
      data: {
        getUser: {
          id: userDb.id,
          email: userDb.email,
          role: "" + userDb.role,
          trips: userDb.trips.map(trip => ({
            destination: trip.destination,
            comment: trip.comment,
            id: trip.id,
            startDate: trip.startDate.toISOString(),
            endDate: trip.endDate.toISOString()
          }))
        }
      }
    });
  }
};

describe("User get", () => {
  it("get User no auth", async () => {
    await getUser(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("get User as user", async () => {
    await getUser(defaults.contexts.user, null);
  });

  it("get User as manager", async () => {
    await getUser(defaults.contexts.manager, null);
  });

  it("get User as admin", async () => {
    await getUser(defaults.contexts.admin, null);
  });

  it("get User different as user", async () => {
    await getUser(
      defaults.contexts.user,
      ERROR_MESSAGE.ACCESS_DENIED,
      defaults.users.different.id
    );
  });

  it("get User different as manager", async () => {
    await getUser(defaults.contexts.manager, null, defaults.users.different.id);
  });

  it("get User different as admin", async () => {
    await getUser(defaults.contexts.admin, null, defaults.users.different.id);
  });

  it("get User as admin", async () => {
    await getUser(
      defaults.contexts.admin,
      ERROR_MESSAGE.NOT_FOUND,
      defaults.users.different.id + 100
    );
  });
});