import "reflect-metadata";
import {createConnection} from "typeorm";
import queryComplexity, { simpleEstimator, fieldExtensionsEstimator } from "graphql-query-complexity";
import express, {Request, Response} from "express";
import graphqlHTTP from "express-graphql";
import expressPlayground from "graphql-playground-middleware-express";
import bodyParser from "body-parser";
import {AppRoutes} from "./routes";
import {seedDatabase} from "./utils/initDatabaseData";
import next from "next";
import env from "dotenv";
import {Context} from "./auth/Context.interface";
import {User} from "./entity/User";
import {createSchema} from "./utils/createSchema";
import jwt from "jsonwebtoken";
import cors from "cors";
import {TOKEN_EXPIRED} from "./constants-shared/constants";

env.config();

const dev = false;//process.env.NODE_ENV !== 'production';
const app = next({ dir:"../frontend", dev });
const handle = app.getRequestHandler();

createConnection().then(async connection => {
    const server = express();

    await seedDatabase();

    const schema = await createSchema();

    if(process.env.NODE_ENV === 'production'){
        // HTTPS redirect
        server.get('*', function (req, res, next) {
            if(req.headers['x-forwarded-proto'] != 'https')
                res.redirect(301, 'https://' + req.hostname + req.url);
            else
                next() // Continue to other routes
        })
    }

    server.use(cors());
    server.use(bodyParser.json());

    AppRoutes.forEach(route => {
        server[route.method](route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response)
                .then(() => next)
                .catch(err => next(err));
        });
    });

    server.use(
        "/graphql",
        graphqlHTTP(async (req: Request, res: Response, params) => ({
            schema,
            context: (() => {
                const authHeader: String = req.headers.authorization;
                if (authHeader) {
                    const token: String = authHeader.split(' ')[1];
                    try {
                        const decoded = jwt.verify(token, process.env.JWT_SECRET_KEY);
                        console.log("ni here", decoded)
                        const ctx: Context = {
                            // create mocked user in context
                            // in real app you would be mapping user from `req.user` or sth
                            user: decoded ? decoded.user : decoded,
                        };
                        return ctx;
                    } catch (err) {
                        if(err instanceof jwt.TokenExpiredError){
                            throw new Error(TOKEN_EXPIRED);
                        }else{
                            console.log("JWT auth err", err)
                        }
                    }
                }
                return {}

            })(),
            validationRules: [
                 // This provides GraphQL query analysis to reject complex queries to your GraphQL server.
                 // This can be used to protect your GraphQL servers
                 // against resource exhaustion and DoS attacks.
                 // More documentation can be found (here)[https://github.com/ivome/graphql-query-complexity]
                queryComplexity({
                    // The maximum allowed query complexity, queries above this threshold will be rejected
                    maximumComplexity: 20,
                    // The query variables. This is needed because the variables are not available
                    // in the visitor of the graphql-js library
                    variables: params!.variables!,
                    // Optional callback function to retrieve the determined query complexity
                    // Will be invoked weather the query is rejected or not
                    // This can be used for logging or to implement rate limiting
                    onComplete: (complexity: number) => {
                        console.log("Query Complexity:", complexity);
                    },
                    // Add any number of estimators. The estimators are invoked in order, the first
                    // numeric value that is being returned by an estimator is used as the field complexity.
                    // If no estimator returns a value, an exception is raised.
                    estimators: [
                        fieldExtensionsEstimator(),
                        // Add more estimators here...
                        // This will assign each field a complexity of 1 if no other estimator
                        // returned a value.
                        simpleEstimator({
                            defaultComplexity: 1,
                        }),
                    ],
                }),
            ],
        })),
    );
    server.get("/playground", expressPlayground({ endpoint: "/graphql" }));

    server.get('*', (req: Request, res: Response) => {
        handle(req, res)
    });
    const PORT = process.env.PORT || 4000;
    server.listen(PORT, () => {
        console.log(
            `Server is running, GraphQL Playground available at http://localhost:${PORT}/playground`,
        );
    });

}).catch(error => console.log("TypeORM connection error: ", error));
