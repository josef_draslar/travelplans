import React from "react";
import RoutesWrapper from "../utils/RoutesWrapper";
import LoadingSpinner from "./LoadingSpinner";

const PageLoader = (props) => (
    <RoutesWrapper
        showHeaderShadow
    >
        <LoadingSpinner />
    </RoutesWrapper>
);

export default PageLoader;