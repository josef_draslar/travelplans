import styled from "styled-components";
import fonts from "../../utils/fonts";
import colors from "../../utils/colors";
import { media } from "../../utils/Media";
import { Label } from "reactstrap";

export const Text = styled.div`
  font-size: 18px;
  text-align: center;
  margin-top: 30px;
`;

export const A = styled.a`
  color: ${props => colors.primary} !important;
  
  &:hover,
  &:focus {
    color: ${props => colors.primaryHover};
  }
`;

export const SmallText = styled.div`
  font-size: 10px;
  text-align: center;
  margin-top: 30px;
`;

export const EmailWrapper = styled.div`
  padding-left: 75px;
  margin-left: -15px;
  max-width: 280px;
  border-left: 1px solid
    ${props => colors.lightGray};
  position: relative;

  ${media.lessThan("mobileLayout")`
    border-left: none;
    border-top: 1px solid ${props => colors.lightGray};
    padding-left: 0;
    margin-left: 0;
    margin-top: 30px;
    padding-top: 20px;
    max-width: unset;
  `};
`;

export const Or = styled.div`
  position: absolute;
  top: 50%;
  left: 0;
  color: ${props => colors.lightGray};
  background-color: ${props => colors.white};
  transform: translateY(-50%) translateX(-50%);
  font-family: ${fonts.mainTitle};

  ${media.lessThan("mobileLayout")`
    top: -12px;
    left: 50%;
    transform: translateX(-50%);
    padding-left: 10px;
    padding-right: 10px;
  `};
`;

export const LabelLocal = styled(Label)`
  font-family: ${fonts.mainTitle};
  margin-bottom: 5px;
`;

export const ButtonWrapper = styled.div`
  text-align: center;
`;

export const H6 = styled.h6`
  text-align: center;
  font-size: 16px;
  font-family: ${fonts.title};
`;

export const SmallTextSpan = styled.span`
  border-top: 1px solid
    ${props => colors.lightGray};
  padding-top: 5px;
  margin-top: 20px;

  ${media.lessThan("sm")`
      display: block;
    `};
`;

export const FormWrapper = styled.div`
  max-width: 350px;
  margin-left: auto;
  margin-right: auto;
`;