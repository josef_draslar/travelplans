import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { Trip } from "../../src/entity/Trip";
import { ExecutionResult } from "graphql/execution/execute";
import { stringifyDatesTrip as stringifyDates } from "../test-utils/stringifyDates";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { TripQueries } from "../../src/constants-shared/TripQueries";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";

env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const getTrip = async (
  context?: Context,
  expectedErrorMessage?: string,
  trip: Trip = defaults.trips.ofCurrent
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: TripQueries.getTrip,
      variableValues: {
        id: trip.id
      }
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
  } else {
    expect(response.errors).not.toBeDefined();
    expect(response).toMatchObject({
      data: {
        getTrip: {
          ...stringifyDates(trip),
          user: {
            email: trip.user.email,
            id: trip.user.id
          }
        }
      }
    });
  }
};

describe("Trip get", () => {
  it("get Trip no auth", async () => {
    await getTrip(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });
  it("get Trip as user", async () => {
    await getTrip(defaults.contexts.user);
  });
  it("get Trip as manager", async () => {
    await getTrip(defaults.contexts.manager, ERROR_MESSAGE.ACCESS_DENIED);
  });
  it("get Trip as admin", async () => {
    await getTrip(defaults.contexts.admin);
  });
  it("get Trip of different user as user", async () => {
    await getTrip(
      defaults.contexts.user,
      ERROR_MESSAGE.ACCESS_DENIED,
      defaults.trips.ofDifferent
    );
  });
  it("get Trip of different user as admin", async () => {
    await getTrip(defaults.contexts.admin, null, defaults.trips.ofDifferent);
  });
  it("get Trip wrong id", async () => {
    await getTrip(defaults.contexts.admin, ERROR_MESSAGE.NOT_FOUND, {
      ...defaults.trips.ofDifferent,
      id: defaults.trips.ofDifferent.id + 100
    });
  });
});