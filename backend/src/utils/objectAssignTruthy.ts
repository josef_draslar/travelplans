export const objectAssignTruthy = (a, b) => {
  const result = { ...a };
  Object.keys(b).forEach(key => {
    if (isTruthy(b[key])) result[key] = b[key];
  });
  return result;
};

const isTruthy = a => typeof a !== 'undefined' && a !== null;
