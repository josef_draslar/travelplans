import React from "react";
import styled from "styled-components";

import { media } from "../../utils/Media";
import colors from "../../utils/colors";
import Button from "../Button";


export const FacebookButton = () => (<FacebookButtonWrapper>
        <Button
            href="/auth/facebook"
            iconKeyFull="fab fa-facebook-f"
            backgroundColor={colors.facebookBlue}
        >
            Facebook
        </Button>
    </FacebookButtonWrapper>);

const FacebookButtonWrapper = styled.span`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translateY(-50%) translateX(-50%);

  ${media.lessThan("mobileLayout")`
    position: relative;
    top: unset;
    left: unset;
    width: 100%;
    display: block;
    transform: none;
    text-align: center;
  `};
`;