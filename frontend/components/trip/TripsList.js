import React, { useMemo, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { useSelector } from "react-redux";
import { withTranslation } from "../../utils/multiLanguageSupport";
import { MustSignInBanner } from "./MustSignIn";
import moment from "moment";
import Card from "./Card";
import Button from "../Button";
import TripFilter from "./TripFilter";
import {normalize} from "../../utils/Utils";

export const enrichTrip = trip => ({
    ...trip,
    startsIn: moment.duration(moment(trip.startDate).diff(moment().add(1, "days").startOf('day'))).asDays()
});

const enrichTrips = (trips, justNextMonth) => {
    if(!trips) return null;
    let localTrips = trips;
    if(justNextMonth) {
        const today = moment().startOf('day');
        const days30 = moment().add(30, "days").endOf('day');
        localTrips = trips.filter(trip=> {
            const endToday = moment.duration(moment(trip.endDate).diff(today)).asDays();
            const days30Start = moment.duration(days30.diff(moment(trip.startDate))).asDays();
            return endToday > 0 && days30Start > 0
        }

        )
    }
    return localTrips.map(trip => enrichTrip(trip));
};

const TripsList = ({ t, justNextMonth }) => {
  const [filter, setFilter] = useState("");
  const trips = useSelector(state => state.user.trips);

  const tripsEnriched = useMemo(()=>enrichTrips(trips, justNextMonth), [trips]);

  if (!trips) return <MustSignInBanner />;

  const filteredTrips = tripsEnriched.filter(({destination})=>{
      console.log(destination, filter)
      return normalize(destination).indexOf(normalize(filter))!==-1
  });

  return (
    <div>
      <ButtonWrapper>
        <Button href="/trip/create" inSiteLink iconKey="plus">
          {t("Trip.add")}
        </Button>
      </ButtonWrapper>
        <TripFilter updateFilters={setFilter} />
      <Cards>
        {filteredTrips.map(trip =>
          <Card
            title={trip.destination}
            subTitle={
              moment(trip.startDate).format("M/D/YY") +
              " - " +
              moment(trip.endDate).format("M/D/YY")
            }
            subTitleSecond={
              trip.startsIn > 0
                ? `${trip.startsIn.toFixed(0)} ${t("Trip.toStart")}`
                : ""
            }
            text={trip.comment}
            url="/trip/detail/[tripId]"
            as={"/trip/detail/" + trip.id}
            btnText={t("Base.more")}
          />
        )}
      </Cards>
    </div>
  );
};

TripsList.propTypes = {
    justNextMonth: PropTypes.bool
};

export default withTranslation(TripsList);

const Cards = styled.div`text-align: center;`;

const ButtonWrapper = styled.div`
  margin-top: 30px;
  text-align: center;
`;
