import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import faker from "faker";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { Trip, TripCreate } from "../../src/entity/Trip";
import { ExecutionResult } from "graphql/execution/execute";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
  expectNumberOfDbInstances
} from "../test-utils/customMatchers";
import { TripMutations } from "../../src/constants-shared/TripMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";

env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const createTripCreate = (userId: number, params: object = {}): TripCreate => ({
  destination: faker.address.city(),
  comment: faker.lorem.sentence(),
  startDate: faker.date.recent(),
  endDate: faker.date.future(),
  userId,
  ...params
});

const createTrip = async (
  context?: Context,
  expectedErrorMessage?: string,
  params: object = {}
) => {
  const trip: TripCreate = createTripCreate(defaults.users.current.id, params);
  const response: ExecutionResult = await getGraphQL(
    {
      source: TripMutations.createTrip,
      variableValues: {
        data: trip
      }
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
    expectNumberOfDbInstances(defaults.repos.trip)(defaults.users.count);
  } else {
    expect(response.errors).not.toBeDefined();
    expect(response).toMatchObject({
      data: { createTrip: { destination: trip.destination } }
    });

    const id: number = response.data.createTrip.id;
    expect(id).toBeTruthy();

    const tripDb: Trip = await defaults.repos.trip.findOne({ where: { id } });
    expect(tripDb).toBeTruthy();
  }
};

describe("Trip create", () => {
  it("create Trip no auth", async () =>
    await createTrip(undefined, ERROR_MESSAGE.ACCESS_DENIED));

  it("create Trip as user", async () =>
    await createTrip(defaults.contexts.user));

  it("create Trip as manager", async () =>
    await createTrip(defaults.contexts.manager, ERROR_MESSAGE.ACCESS_DENIED));

  it("create Trip as admin", async () =>
    await createTrip(defaults.contexts.admin));

  it("create Trip without comment", async () =>
    await createTrip(defaults.contexts.user, null, {
      comment: null
    }));

  it("create Trip startDate after endDate", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.START_DATE_AFTER_END, {
      startDate: faker.date.future(),
      endDate: faker.date.recent(),
    }));

  it("create Trip missing destination", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.INVALID_NULL, {
      destination: null
    }));

  it("create Trip missing startDate", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.INVALID_NULL, {
      startDate: null
    }));

  it("create Trip missing endDate", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.INVALID_NULL, {
      endDate: null
    }));

  it("create Trip missing userID", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.INVALID_NULL, {
      userId: null
    }));

  it("create Trip for different user as user", async () =>
    await createTrip(defaults.contexts.user, ERROR_MESSAGE.ACCESS_DENIED, {
      userId: defaults.users.different.id
    }));

  it("create Trip for different user as admin", async () =>
    await createTrip(defaults.contexts.admin, ERROR_MESSAGE.ACCESS_DENIED, {
      userId: defaults.users.different.id
    }));
});