import {AuthChecker} from "type-graphql";
import {Context} from "./Context.interface";
import {Role, UserRead} from "../entity/User";

export const customAuthChecker: AuthChecker<Context, Role> = (
    { context },
    roles: Array<Role>,
) => {
    const user: UserRead = context.user;
    if (roles.length === 0) {
        // if `@Authorized()`, check only is user exist
        return user !== undefined;
    }
    // there are some roles defined now

    if (!user) {
        return false;
    }

    if (roles.includes(user.role)) {
        // grant access if the roles overlap
        return true;
    }

    // no roles matched, restrict access
    return false;
};