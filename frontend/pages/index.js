import React from "react";
import TripsList from "../components/trip/TripsList";
import RoutesWrapper from "../utils/RoutesWrapper";
import {PageTitle} from "../components/BasicElements";
import {withTranslation} from "../utils/multiLanguageSupport";

const HomePage = ({t}) => (
      <RoutesWrapper
        showHeaderShadow
      >
            <PageTitle>{t("Title./")}</PageTitle>
            <TripsList />
      </RoutesWrapper>
    );

export default withTranslation(HomePage);