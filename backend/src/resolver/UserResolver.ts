import {Resolver, Mutation, Query, Arg, Authorized, Ctx} from "type-graphql";
import {Role, User, UserCreate, UserRead, UserUpdate, UserWithToken} from "../entity/User";
import {getConnection, getManager, Repository} from "typeorm";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import {Context} from "../auth/Context.interface";
import {
  ACCESS_DENIED, DEFAULT_PASSWORD,
  EMAIL_NOT_VALID, NOT_FOUND, PASSWORD_TOO_WEAK, USER_EMAIL_NOT_EXIST, USER_EXIST_ERR,
  USER_PASSWORD_NOT_MATCH
} from "../constants-shared/constants";
import {Trip} from "../entity/Trip";

@Resolver()
export class UserResolver {
  private repo: Repository<User>;

  constructor() {
    this.repo = getManager().getRepository(User);
  }

  private static createToken(user: User): String {
    //TODO IMPROVEMENT: create token db entity, upon user creation store unique user token, upon authentication verify it with bear from client, refresh upon token expired
    const DAYS_10_IN_SEC: number = 10*24*60*60;
    return jwt.sign( {user}, process.env.JWT_SECRET_KEY, {expiresIn: DAYS_10_IN_SEC} )
  }

  @Authorized<Role>(Role.user_manager, Role.admin)
  @Query(() => [UserRead])
  getUsers() {
    //TODO IMPROVEMENT: add pagination
    return this.repo.find();
  }

  @Authorized<Role>(Role.user, Role.user_manager, Role.admin)
  @Query(() => UserRead)
  async getUser(@Arg("id") id: number, @Ctx() ctx: Context) {
    const userCtx: UserRead = ctx.user;

    if(userCtx.role === Role.user && userCtx.id!==id)
      throw new Error(ACCESS_DENIED);

    const user = await this.repo.findOne({ where: { id }, relations: ["trips"] });

    if (!user) throw new Error(NOT_FOUND);

    return user;
  }

  @Mutation(() => UserWithToken)
  async logInUser(@Arg("data") data: UserCreate, @Ctx() ctx: Context) {
    const user = await this.repo.findOne({ where: { email: data.email }, relations: ["trips"] });

    if (!user)
      throw new Error(USER_EMAIL_NOT_EXIST);

    if ( !bcrypt.compareSync( data.password, user.password )) {
      throw new Error(USER_PASSWORD_NOT_MATCH);
    }

    user.password = undefined;
    const token = UserResolver.createToken({...user,trips:undefined});
    return {
      user: user,
      token: token,
    };
  }

  @Mutation(() => UserWithToken)
  async createUser(@Arg("data") data: UserCreate) {
    const u = await this.repo.findOne({ where: { email: data.email } });

    if (!!u)
      throw new Error(USER_EXIST_ERR);

    const emailVerification = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!emailVerification.test(data.email)) throw new Error(EMAIL_NOT_VALID);

    // > 8 chars AND uppercase AND lowercase AND number AND special character
    //const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
    // > 6 chars AND uppercase and lowercase OR letter and number
    const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
    if(!mediumRegex.test(data.password))
      throw new Error(PASSWORD_TOO_WEAK);

    let user = this.repo.create({
      email: data.email,
      password: bcrypt.hashSync(data.password, 10)
    });

    await this.repo.save(user);

    user = await this.repo.findOne({ where: { email: data.email } });

    user.password = undefined;
    const token = UserResolver.createToken({...user,trips:undefined});

    return {
      user,
      token
    };
  }

  @Authorized<Role>(Role.user, Role.user_manager, Role.admin)
  @Mutation(() => UserRead)
  async updateUser(@Arg("data") data: UserUpdate, @Ctx() ctx: Context) {
    const contextUser: UserRead = ctx.user;
    if(contextUser.role === Role.user && contextUser.id!==data.id)
      throw new Error(ACCESS_DENIED);

    const user = await this.repo.findOne({ where: { id: data.id } });
    if (!user) throw new Error(NOT_FOUND);

    if(contextUser.role === Role.user_manager && user.role === Role.admin)
      throw new Error(ACCESS_DENIED);

    if(data.email){
      user.email = data.email;
    }

    await this.repo.save(user);
    return user;
  }

  @Authorized<Role>(Role.admin)
  @Mutation(() => UserRead)
  async changeRoleUser(@Arg("id") id: number, @Ctx() ctx: Context) {
    //TODO cover by tests

    const user = await this.repo.findOne({ where: { id } });
    if (!user) throw new Error(NOT_FOUND);

    if (user.role===0) throw new Error(ACCESS_DENIED);

    user.role = user.role===1?2:1;

    await this.repo.save(user);
    return user;
  }

  @Authorized<Role>(Role.user_manager, Role.admin)
  @Mutation(() => Boolean)
  async resetUserPassword(@Arg("id") id: number, @Ctx() ctx: Context) {
    //TODO cover by tests
    const contextUser: UserRead = ctx.user;

    const user = await this.repo.findOne({ where: { id } });
    if (!user) throw new Error(NOT_FOUND);

    if(contextUser.role === Role.user_manager && user.role === Role.admin)
      throw new Error(ACCESS_DENIED);

    user.password = bcrypt.hashSync(DEFAULT_PASSWORD, 10);

    await this.repo.save(user);
    return true;
  }

  @Authorized<Role>(Role.user, Role.user_manager, Role.admin)
  @Mutation(() => Boolean)
  async deleteUser(@Arg("id") id: number, @Ctx() ctx: Context) {
    const contextUser: UserRead = ctx.user;
    if(contextUser.role === Role.user && contextUser.id!==id)
      throw new Error(ACCESS_DENIED);

    const user = await this.repo.findOne({ where: { id } });
    if (!user) throw new Error(NOT_FOUND);

    if(contextUser.role === Role.user_manager && user.role === Role.admin)
      throw new Error(ACCESS_DENIED);

    await getConnection()
        .createQueryBuilder()
        .delete()
        .from(Trip)
        .where("user_id = :userId", { userId: id })
        .execute();
    await this.repo.remove(user);
    return true;
  }
}
