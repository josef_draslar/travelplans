import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import RoutesWrapper from "../../utils/RoutesWrapper";
import {withTranslation} from "../../utils/multiLanguageSupport";

const PrivacyPolicy = ({t}) => {
    return (
      <RoutesWrapper
        location={"/static/terms-of-service"}
        showHeaderShadow
      >
        <div>
          <P>{t("Join.policy")}</P>
        </div>
      </RoutesWrapper>
    );
};

PrivacyPolicy.propTypes = {
    t: PropTypes.object.isRequired
};

export default withTranslation(PrivacyPolicy);

const P = styled.p`
  padding: 40px;
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
`;