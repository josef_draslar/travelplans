import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Navbar, NavbarBrand } from "reactstrap";
import Link from "next/link";
import styled, { css } from "styled-components";
import colors from "../../utils/colors";
import { media } from "../../utils/Media";
import { logOutUser } from "../../redux/actions/UserActions";
import { fireLocalAction } from "../../redux/actions/LocalActions";
import Join from "../join/Join";
import { setNotificationMessageCallback } from "../NotificationMessage";
import NotificationMessage from "../NotificationMessage";
import NavMobile from "./NavMobile";
import NavMain from "./NavMain";
import { isUserAdmin, isUserManager } from "../../utils/Utils";
import { localActions } from "../../redux/reducers/LocalReducer";
import { withTranslation } from "../../utils/multiLanguageSupport";

class NavBar extends React.PureComponent {
  state = {
    isOpen: false,
    notificationMessage: null,
    fixed: false
  };

  displayNotificationMessage = message => {
    this.setState({
      notificationMessage: (
        <NotificationMessage messageObject={message} t={this.props.t} />
      )
    });
    setTimeout(() => this.setState({ notificationMessage: null }), 8000);
  };

  componentDidMount() {
    setNotificationMessageCallback(this.displayNotificationMessage);

    document.addEventListener("scroll", this.scroll);
  }

  componentWillUnmount() {
    document.addEventListener("scroll", undefined);
  }

  scroll = () => {
    const browserHeight =
      (isNaN(window.innerHeight) ? window.clientHeight : window.innerHeight) ||
      document.documentElement
        ? document.documentElement.clientHeight
        : 0;
    const scrollFromTop =
      document.body.scrollTop || document.documentElement.scrollTop;
    const { fixed } = this.state;

    const comparison = scrollFromTop - browserHeight;
    if (comparison > 0 && !fixed) {
      this.setState({ fixed: true });
    } else if (comparison <= 0 && fixed) {
      this.setState({ fixed: false });
    }
  };

  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  openSignIn = () => {
    this.toggle();

    this.props.fireLocalAction(localActions.OPEN_SIGN_IN);
  };

  openSignUp = () => {
    this.toggle();
    this.props.fireLocalAction(localActions.OPEN_SIGN_UP);
  };

  logOutUser = () => {
    this.toggle();
    this.props.logOutUser();
  };

  render() {
    const { isOpen, notificationMessage, fixed } = this.state;
    const {
      userToken,
      navBarWhite,
      showHeaderShadow,
      showBorder,
      user,
      t
    } = this.props;

    const isAdmin = isUserAdmin(user);
    const isManger = isUserManager(user);
    return (
      <Wrapper>
        <NavBarWrapper>
          <NavbarStyled
            fixed={fixed}
            showBorder={showBorder}
            showHeaderShadow={showHeaderShadow}
          >
            <ContainerLocal>
              <NavBarBrandStyled tag={Link} href="/">
                <Logo src="/static/logo.png" alt="Travel Plan" />
              </NavBarBrandStyled>
              <NavMain
                userToken={userToken}
                navBarWhite={!fixed && navBarWhite}
                openSignUp={this.props.openSignUp}
                openSignIn={this.props.openSignIn}
                logOutUser={this.props.logOutUser}
                t={t}
              />
              <MenuBars
                onClick={this.toggle}
                className={isOpen ? "isOpen" : ""}
                showAlways={isManger || isAdmin}
              >
                <Bar
                  color={!fixed && navBarWhite ? colors.white : colors.font}
                />
                <Bar
                  color={!fixed && navBarWhite ? colors.white : colors.font}
                />
                <Bar
                  color={!fixed && navBarWhite ? colors.white : colors.font}
                />
              </MenuBars>
              <NavMobile
                isOpen={isOpen}
                userToken={userToken}
                openSignIn={this.openSignIn}
                openSignUp={this.openSignUp}
                logOutUser={this.logOutUser}
                toggle={this.toggle}
                t={t}
                isAdmin={isAdmin}
                isManger={isManger}
              />
            </ContainerLocal>
          </NavbarStyled>
        </NavBarWrapper>
        <Container>
          <Join />
          {notificationMessage && notificationMessage}
        </Container>
      </Wrapper>
    );
  }
}

NavBar.propTypes = {
  navBarWhite: PropTypes.bool,
  showBorder: PropTypes.bool,
  showHeaderShadow: PropTypes.bool
};

export default withTranslation(
  connect(
    state => ({
      userToken: state.user.token,
      user: state.user.user
    }),
    {
      logOutUser,
      fireLocalAction
    }
  )(NavBar)
);

const NavBarBrandStyled = styled(NavbarBrand)`
  height: 68px !important;
  padding-top: 0;
  color: ${colors.white};
  font-size: 35px;
  font-weight: 600;
  position: relative;
  margin-right: 0;
  margin-left: 50px;

  ${media.lessThan("sm")`
    margin-left: auto;
    margin-right: auto;
  `};
`;

const NavBarWrapper = styled.div`
  z-index: 101;
  padding: 0;
  height: 106px;
  position: relative;

  ${media.lessThan("sm")`height: 69px;`};
`;

const NavbarStyled = styled(
  ({ fixed, showBorder, showHeaderShadow, ...rest }) => <Navbar {...rest} />
)`
  z-index: 101;
  padding: 0;
  height: 100%;
  position: unset;
  top: -106px;
  min-width: 320px;

  -webkit-transition: top 0.5s linear;
  -moz-transition: top 0.5s linear;
  transition: top 0.5s linear;

  ${props =>
    props.showHeaderShadow && `box-shadow: 0 0 13px 3px rgba(0,0,0,.05);`}
    
    
  ${props =>
    props.showBorder &&
    css`border-bottom: 1px solid ${colors.lightestGray}; background: rgba(0,0,0,0.09); ${media.lessThan(
      "sm"
    )`
      background: ${colors.white};
    `}`};
    
    ${props =>
      props.fixed &&
      css`
      position: fixed;
     top: 0;
    left: 0;
    right: 0;
    height: 106px;
    background: ${colors.white};
    box-shadow: 0 0 13px 3px rgba(0,0,0,.05);
    ${media.lessThan("sm")`
      height: 70px;
    `}
  `};

`;

const Wrapper = styled.div`background: ${colors.white};`;

const Logo = styled.img`
  width: 127px;
  margin-left: 0;
  margin-top: 44px;

  &:hover {
    cursor: pointer;
  }

  ${media.lessThan("sm")`
    margin-top: 24px;
  `};
`;

const Bar = styled.div`
  background-color: ${props => props.color};
  position: absolute;
  width: 100%;
  height: 2px;
  border-radius: 1px;

  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;

  &:first-child {
    top: 0;
  }

  &:nth-child(2) {
    top: 6px;
  }

  &:last-child {
    bottom: 0;
  }

  ${media.lessThan("sm")`
    background-color: ${colors.font};
  `};
`;

const MenuBars = styled.div`
  width: 17px;
  height: 14px;
  position: absolute;
  right: 10px;
  z-index: 1;
  top: 46px;
  display: none;
  ${media.lessThan("960px")`
    display: block;
  `};

  ${props => props.showAlways &&"display: block;"}; 
  
  ${media.lessThan("sm")`
  top: 28px;
  `};

  &:hover,
  &:focus {
    cursor: pointer;
  }

  &.isOpen {
    ${Bar}:first-child {
      transform: translate(-90px) rotate(45deg);
      ${media.lessThan("md")`
        transform: translate(90px) rotate(45deg);
      `};
      opacity: 0;
    }

    ${Bar}:nth-child(2) {
      opacity: 0;
    }

    ${Bar}:last-child {
      transform: translate(-90px) rotate(-45deg);
      ${media.lessThan("md")`
        transform: translate(90px) rotate(-45deg);
      `};
      opacity: 0;
    }
  }

  ${media.lessThan("md")`
    right: auto;
    left: 10px;
  `};
`;

const ContainerLocal = styled.div`
  height: 100%;
  max-width: 1050px;
  margin-left: auto;
  margin-right: auto;

  ${media.lessThan("355px")`
        padding-left: 30px;
  `};
`;
