import {Resolver, Mutation, Query, Arg, Authorized, Ctx} from "type-graphql";
import {Trip, TripCreate, TripUpdate} from "../entity/Trip";
import {getManager, Repository} from "typeorm";
import {Role, UserRead} from "../entity/User";
import {Context} from "../auth/Context.interface";
import {ACCESS_DENIED, NOT_FOUND, START_DATE_AFTER_END} from "../constants-shared/constants";
import {objectAssignTruthy} from "../utils/objectAssignTruthy";

@Resolver(of => Trip)
export class TripResolver {
    private repo: Repository<Trip>;

    constructor(){
        this.repo = getManager().getRepository(Trip);
    }

    @Authorized<Role>(Role.user, Role.admin)
    @Query(() => [Trip])
    getTrips(@Ctx() ctx: Context) {
        //TODO IMPROVEMENT (IF USER HAS LARGE AMOUNT OF TRIPS): add pagination, return just few trips (first page?) upon user log in, sign up and get
        //TODO IMPROVEMENT (IF USER HAS LARGE AMOUNT OF TRIPS): add date filters
        const user: UserRead = ctx.user;
        let where = {};
        if(user.id && user.role === Role.user)
            where = {user:{id:user.id}};

        return this.repo.find({ where, relations: ["user"]})
    }

    @Authorized<Role>(Role.user, Role.admin)
    @Mutation(() => Trip)
    async createTrip(@Arg("data") data: TripCreate, @Ctx() ctx: Context) {
        const user: UserRead = ctx.user;
        if(user.id!==data.userId)
            throw new Error(ACCESS_DENIED);
        if(data.startDate.getTime()>data.endDate.getTime())
            throw new Error(START_DATE_AFTER_END);

        const trip = this.repo.create(data);
        await this.repo.save(trip);
        return trip;
    }

    @Authorized<Role>(Role.user, Role.admin)
    @Query(() => Trip)
    async getTrip(@Arg("id") id: number, @Ctx() ctx: Context) {
        const trip: Trip = await this.repo.findOne({ where: { id }, relations: ["user"]});
        if (!trip) throw new Error(NOT_FOUND);

        const user: UserRead = ctx.user;
        if(user.role === Role.user && user.id!==trip.user.id)
            throw new Error(ACCESS_DENIED);

        return trip;
    }

    @Authorized<Role>(Role.user, Role.admin)
    @Mutation(() => Trip)
    async updateTrip(@Arg("data") data: TripUpdate, @Ctx() ctx: Context) {
        const trip = await this.repo.findOne({ where: { id: data.id }, relations: ["user"] });
        if (!trip) throw new Error(NOT_FOUND);
        const user: UserRead = ctx.user;

        if(user.role === Role.user && user.id!==trip.user.id)
            throw new Error(ACCESS_DENIED);
        const newTrip = objectAssignTruthy(trip, data);
        if(newTrip.startDate.getTime()>newTrip.endDate.getTime())
            throw new Error(START_DATE_AFTER_END);

        await this.repo.save(newTrip);
        return newTrip;
    }

    @Authorized<Role>(Role.user, Role.admin)
    @Mutation(() => Boolean)
    async deleteTrip(@Arg("id") id: number, @Ctx() ctx: Context) {
        const trip = await this.repo.findOne({ where: { id }, relations: ["user"] });
        if (!trip) throw new Error(NOT_FOUND);

        const user: UserRead = ctx.user;
        if(user.role === Role.user && user.id!==trip.user.id)
            throw new Error(ACCESS_DENIED);


        await this.repo.remove(trip);
        return true;
    }
}