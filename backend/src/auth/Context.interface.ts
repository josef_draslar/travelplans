import { UserRead } from "../entity/User";

export interface Context {
    user?: UserRead;
}