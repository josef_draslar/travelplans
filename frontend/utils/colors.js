const colors = {
  primary: "#209CEE",
  primaryBackground: "#1a84c3",
  primaryHover: "#0889ee",

  font: "#1c1c1c",
  background: "white",

  middleDark: "#29282d",
  lightGray: "#6D6D6D",
  lighterGray: "#c8c8c8",
  lightestGray: "#d8d8d8",
  superLightestGray: "#f1f1f1",
  white: "white",
  facebookBlue: "#4065b4",

  error: "#d62536",
  errorHover: "#9c242e",

  inputColor: "#313646",
  inputBcgColor: "#f1f1f1"
};

export default colors;
