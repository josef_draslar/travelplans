import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import faker from "faker";
import env from "dotenv";
import {
  User,
  UserCreate,
} from "../../src/entity/User";
import { UserMutations } from "../../src/constants-shared/UserMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
  expectNumberOfDbInstances
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const stringifyId = obj => ({ ...obj, id: "" + obj.id });

const createUser = async (
  expectedErrorMessage?: string,
  params: object = {}
) => {
  const user: UserCreate = {
    email: faker.internet.email(),
    password: faker.internet.password(6, false, null, "aA"),
    ...params
  };
  const response: ExecutionResult = await getGraphQL({
    source: UserMutations.createUser,
    variableValues: {
      data: user
    }
  });

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
    expectNumberOfDbInstances(defaults.repos.user)(defaults.users.count);
  } else {
    const expected = {
      user: {
        id: expect.stringMatching(/^[0-9]*$/),
        email: user.email
      },
      token: expect.stringMatching(/[^']{23,}?/)
    };
    expect(response.errors).not.toBeDefined();
    expect({
      data: {
        createUser: {
          ...response.data.createUser,
          user: stringifyId(response.data.createUser.user)
        }
      }
    }).toMatchObject({
      data: { createUser: { ...expected } }
    });

    const expectedUser = {
      ...expected.user,
      password: expect.stringMatching(/[^']{23,}?/)
    };
    const userDb: User = await defaults.repos.user.findOne({
      where: { id: response.data.createUser.user.id }
    });
    expect(stringifyId(userDb)).toMatchObject(expectedUser);
  }
};

describe("User Create", () => {
  it("create User", async () => {
    await createUser();
  });

  it("create User fault email", async () => {
    await createUser(ERROR_MESSAGE.EMAIL_NOT_VALID, {
      email: "email@email."
    });
  });

  it("create User short password", async () => {
    await createUser(ERROR_MESSAGE.PASSWORD_TOO_WEAK, {
      password: faker.internet.password(5, false, null, "aA")
    });
  });

  it("create User weak password", async () => {
    await createUser(ERROR_MESSAGE.PASSWORD_TOO_WEAK, {
      password: "AAAAAA"
    });
  });

  it("create User missing email", async () => {
    await createUser(ERROR_MESSAGE.INVALID_NULL, { email: null });
  });

  it("create User missing password", async () => {
    await createUser(ERROR_MESSAGE.INVALID_NULL, { password: null });
  });

  it("create User email already exists", async () => {
    await createUser(ERROR_MESSAGE.USER_EXIST_ERR, {
      email: defaults.users.current.email
    });
  });
});