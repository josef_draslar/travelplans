const getTrip = `
  query GetTrip($id: Float!){
      getTrip(id: $id){
          id,
          destination,
          startDate,
          endDate,
          comment,
          user {
            id,
            email
          }
      }
    }
`;

const getTrips = `
  query GetTrips {
      getTrips {
          id,
          destination,
          startDate,
          endDate,
          comment,
          user {
            id,
            email
          }
      }
    }
`;

export const TripQueries = {
    getTrip,
    getTrips
};