import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import { InputType, Field, ObjectType } from "type-graphql";
import {User, UserRead} from "./User";

@ObjectType("Trip")
@Entity()
export class Trip {

    @Field()
    @PrimaryGeneratedColumn()
    id: number;

    @Field()
    @Column()
    destination: string;

    @Field({nullable: true})
    @Column({nullable: true})
    comment?: string;

    @Field()
    @Column()
    startDate: Date;

    @Field()
    @Column()
    endDate: Date;

    @Field(()=>UserRead)
    @ManyToOne(type => User, user => user.trips)
    user: User;

    constructor(destination: string, comment: string, startDate: Date, endDate: Date, user: User) {
        this.destination = destination;
        this.comment = comment;
        this.startDate = startDate;
        this.endDate = endDate;
        this.user = user;
    }

    static createMultiple(trips: Array<{destination: string, comment: string, startDate: Date, endDate: Date, user: User}>):Array<Trip> {
        return trips.map(({destination, comment, startDate, endDate, user})=>new Trip(destination,comment,startDate,endDate, user))
    }
}

@InputType()
export class TripCreate {
    @Field()
    destination: string;

    @Field({nullable: true})
    comment?: string;

    @Field()
    startDate: Date;

    @Field()
    endDate: Date;

    @Field()
    userId: number;

    constructor(destination: string, comment: string, startDate: Date, endDate: Date, userId: number) {
        this.destination = destination;
        this.comment = comment;
        this.startDate = startDate;
        this.endDate = endDate;
        this.userId = userId;
    }
}

@InputType()
export class TripUpdate {
    @Field()
    id: number;

    @Field({nullable: true})
    destination?: string;

    @Field({nullable: true})
    comment?: string;

    @Field({nullable: true})
    startDate?: Date;

    @Field({nullable: true})
    endDate?: Date;

    constructor(destination: string, comment: string, startDate: Date, endDate: Date, id: number) {
        this.destination = destination;
        this.comment = comment;
        this.startDate = startDate;
        this.endDate = endDate;
        this.id = id;
    }
}