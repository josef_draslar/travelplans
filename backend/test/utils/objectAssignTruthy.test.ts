import {objectAssignTruthy} from "../../src/utils/objectAssignTruthy";

describe("objectAssignTruthy", () => {
    it("objectAssignTruthy", () => {
        const temp = {a:"a",b:"b"};
        expect(objectAssignTruthy(temp, {a:undefined, b:null})).toMatchObject(temp);
        expect(objectAssignTruthy(temp, {a:"c"})).toMatchObject({a:"c",b:"b"});
        expect(objectAssignTruthy(temp, {c: "c"})).toMatchObject(temp);
    });
});