import React from "react";
import RoutesWrapper from "../../utils/RoutesWrapper";
import {PageTitle} from "../../components/BasicElements";
import TripsList from "../../components/trip/TripsList";
import {withTranslation} from "../../utils/multiLanguageSupport";

const TripItineraryPage = ({t}) => (
      <RoutesWrapper
        showHeaderShadow
      >
        <PageTitle>{t("NavBar.itinerary_page")}</PageTitle>
        <TripsList justNextMonth />
      </RoutesWrapper>
    );

export default withTranslation(TripItineraryPage);