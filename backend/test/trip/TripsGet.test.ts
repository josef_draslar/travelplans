import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { UserUpdate } from "../../src/entity/User";
import { ExecutionResult } from "graphql/execution/execute";
import { stringifyDatesTrip as stringifyDates } from "../test-utils/stringifyDates";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { TripQueries } from "../../src/constants-shared/TripQueries";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";

env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

interface TripTest {
  destination: string;
  comment?: string;
  startDate: Date;
  endDate: Date;
  user: UserUpdate;
}

const getTrips = async (
  context?: Context,
  expectedErrorMessage?: string,
  expectedResult: Array<TripTest> = [
    {
      ...defaults.trips.ofCurrent,
      user: {
        id: defaults.trips.ofCurrent.user.id,
        email: defaults.trips.ofCurrent.user.email
      }
    },
    {
      ...defaults.trips.ofDifferent,
      user: {
        id: defaults.trips.ofDifferent.user.id,
        email: defaults.trips.ofDifferent.user.email
      }
    }
  ]
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: TripQueries.getTrips,
      variableValues: {}
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
  } else {
    expect(response.errors).not.toBeDefined();
    expect(response.data.getTrips.length).toBe(expectedResult.length);
    expect(response).toMatchObject({
      data: {
        getTrips: expectedResult.map(t => stringifyDates(t))
      }
    });
  }
};

describe("Trips get", () => {
  it("get Trips no auth", async () => {
    await getTrips(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("get Trips as user", async () => {
    await getTrips(defaults.contexts.user, null, [
      {
        ...defaults.trips.ofCurrent,
        user: {
          id: defaults.trips.ofCurrent.user.id,
          email: defaults.trips.ofCurrent.user.email
        }
      }
    ]);
  });

  it("get Trips as manager", async () => {
    await getTrips(defaults.contexts.manager, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("get Trips as admin", async () => {
    await getTrips(defaults.contexts.admin);
  });
});