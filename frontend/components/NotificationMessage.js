import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

let notificationMessageCallback = null;

export const setNotificationMessageCallback = cb =>
  (notificationMessageCallback = cb);

let notificationMessageInnerCallback = {};

export const setNotificationMessageInnerCallback = (tag, cb) =>
  (notificationMessageInnerCallback.tag = cb);

export const removeNotificationMessageInnerCallback = tag =>
  (notificationMessageInnerCallback = {
    ...notificationMessageInnerCallback,
    tag: undefined
  });

export const pushNotificationMessageIfPossible = message => {
  setTimeout(() => {
    const innerCallbacks = Object.values(
      notificationMessageInnerCallback
    ).filter(v => !!v);

    if (innerCallbacks.length > 0) return innerCallbacks[0](message);

    if (!notificationMessageCallback) return;

    notificationMessageCallback(message);
    window.scrollTo(0, 0);
  }, 500);
};

const NotificationMessage = ({ messageObject, t }) => {
  const [show, setShow] = useState(true);

  const close = () => setShow(false);

  if (!messageObject || !show) return null;

  const { title, message, type, translate } = messageObject;
  console.log(title, translate, t(title))
  return (
    <div
      style={{ position: "relative" }}
      className={"alert alert-" + (type === "ERROR" ? "danger" : "success")}
    >
      <CloseIcon className="fas fa-times" onClick={close} />
      {title &&
        <h6>
            {translate ? t(title) : title}
        </h6>}
      {message ? translate ? t(message) : message : null}
    </div>
  );
};

NotificationMessage.propTypes = {
  t: PropTypes.object.isRequired,
  messageObject: PropTypes.object.isRequired
};

export default NotificationMessage;

const CloseIcon = styled.i`
  position: absolute;
  right: 10px;
  top: 3px;
  font-size: 14px;
`;
