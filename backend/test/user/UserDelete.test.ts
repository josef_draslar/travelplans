import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import {
  User,
} from "../../src/entity/User";
import { UserMutations } from "../../src/constants-shared/UserMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
  expectNumberOfDbInstances
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const deleteUser = async (
  context?: Context,
  expectedErrorMessage?: string,
  id: number = defaults.users.current.id
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: UserMutations.deleteUser,
      variableValues: {
        id
      }
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
    expectNumberOfDbInstances(defaults.repos.user)(defaults.users.count);
  } else {
    expect(response.errors).not.toBeDefined();
    expect(response).toMatchObject({
      data: {
        deleteUser: true
      }
    });

    expectNumberOfDbInstances(defaults.repos.user)(defaults.users.count-1);

    expectNumberOfDbInstances(defaults.repos.trip)(defaults.trips.count-1);

    const userDb: User = await defaults.repos.user.findOne({
      where: { id }
    });

    expect(userDb).not.toBeDefined();
  }
};

describe("User delete", () => {
  it("delete User no auth", async () => {
    await deleteUser(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("delete User as user", async () => {
    await deleteUser(defaults.contexts.user);
  });

  it("delete User as manager", async () => {
    await deleteUser(defaults.contexts.manager);
  });

  it("delete User as admin", async () => {
    await deleteUser(defaults.contexts.admin);
  });

  it("delete User different as user", async () => {
    await deleteUser(defaults.contexts.user, ERROR_MESSAGE.ACCESS_DENIED, defaults.users.different.id);
  });

  it("delete User different as manager", async () => {
    await deleteUser(defaults.contexts.manager, null, defaults.users.different.id);
  });

  it("delete User different as admin", async () => {
    await deleteUser(defaults.contexts.admin, null, defaults.users.different.id);
  });

  it("delete User non existing", async () => {
    await deleteUser(defaults.contexts.admin, ERROR_MESSAGE.NOT_FOUND, defaults.users.different.id+100);
  });
});