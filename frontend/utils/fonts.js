const fonts = {
  mainTitle: "'Lato', sans-serif",
  title: "'Raleway', sans-serif",
  text: "'Raleway', sans-serif"
};

export default fonts;
