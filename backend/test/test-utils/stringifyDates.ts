export const stringifyDatesTrip = obj => ({
    ...obj,
    startDate: obj.startDate ? obj.startDate.toISOString() : obj.startDate,
    endDate: obj.endDate ? obj.endDate.toISOString() : obj.endDate
});