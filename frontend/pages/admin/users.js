import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import RoutesWrapper from "../../utils/RoutesWrapper";
import { withTranslation } from "../../utils/multiLanguageSupport";
import { PageTitle } from "../../components/BasicElements";
import { useDispatch, useSelector } from "react-redux";
import {
  changeRoleUser,
  deleteUser,
  getUsers,
  resetUserPassword
} from "../../redux/actions/UserActions";
import {
  getError,
  getIsLoading,
  initLoading,
  isUserAdmin,
  isUserManager
} from "../../utils/Utils";
import MustSignInPage from "../../components/trip/MustSignIn";
import { Table, Button } from "reactstrap";
import { pushNotificationMessageIfPossible } from "../../components/NotificationMessage";
import colors from "../../utils/colors";
import { constants } from "../../utils/constantsHelper";
import PageLoader from "../../components/PageLoader";

const User = ({ t, user, dispatch, isAdmin }) => {
  const [isLoadingPass, setLoadingPass] = useState(false);
  const [isLoadingRemove, setLoadingRemove] = useState(false);
  const [isLoadingRole, setLoadingRole] = useState(false);

  if (!user) return null;

  const onCompleteRole = p => {
    if (p === false) {
      pushNotificationMessageIfPossible({
        title: t("User.roleChanged"),
        type: "SUCCESS"
      });
    }
    setLoadingRole(p);
  };

  const onCompleteRemove = p => {
    if (p === false) {
      pushNotificationMessageIfPossible({
        title: t("User.removed"),
        type: "SUCCESS"
      });
    }
    setLoadingRemove(p);
  };

  const onCompletePass = p => {
    if (p === false) {
      pushNotificationMessageIfPossible({
        title: t("User.changedPassword") + " " + constants.DEFAULT_PASSWORD,
        type: "SUCCESS"
      });
    }
    setLoadingPass(p);
  };

  const roleChange = e =>
    dispatch(changeRoleUser(user.id, initLoading(onCompleteRole)));
  const edit = e =>
    dispatch(resetUserPassword(user.id, initLoading(onCompletePass)));
  const remove = e =>
    dispatch(deleteUser(user.id, initLoading(onCompleteRemove)));
  const error = getError(isLoadingPass) || getError(isLoadingRemove) || getError(isLoadingRole);
  if (error) {
    pushNotificationMessageIfPossible({
      title: error,
      type: "ERROR"
    });
  }

  return (
    <tr>
      <td>
        {user.id}
      </td>
      <td>
        {user.email}
      </td>
      <td>
        {isUserAdmin(user)?"admin":isUserManager(user)?"manager":"user"}
      </td>
      <td>
        {!!isAdmin &&
          <Button
            isLoading={getIsLoading(isLoadingRole)}
            onClick={roleChange}
          >
            {getIsLoading(isLoadingRole)?<LoadingSpinnerInner/>:t(
              isUserManager(user)
                ? "User.changeRoleToUser"
                : "User.changeRoleToManager"
            )}
          </Button>}
      </td>
      <td>
        <Button
          isLoading={getIsLoading(isLoadingPass)}
          onClick={edit}
        >
          {getIsLoading(isLoadingPass)?<LoadingSpinnerInner/>:t("User.resetPassword")}
        </Button>
      </td>
      <td>
        <Button
          onClick={remove}
          disabled={getIsLoading(isLoadingRemove)}
        >
          {getIsLoading(isLoadingRemove)?<LoadingSpinnerInner/>:t("Base.delete")}
        </Button>
      </td>
    </tr>
  );
};

User.propTypes = {
  t: PropTypes.object.isRequired,
  user: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool.isRequired
};

const AdminUsersPage = ({ t }) => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.user.user);
  const users = useSelector(state => state.user.users);

  useEffect(() => dispatch(getUsers()), []);

  if (!user || !user.email || (!(isUserAdmin(user) && !isUserManager(user))))
    return <MustSignInPage text={t("Admin.signIn")} />;

  if (!users)
    return <PageLoader />;

  const filteredUsers = isUserManager(user)
    ? users.filter(u => !isUserAdmin(u) && u.id !== user.id)
    : users.filter(u => u.id !== user.id);

  return (
    <RoutesWrapper showHeaderShadow>
      <PageTitle>
        {t("NavBar.admin_users_page")}
      </PageTitle>
      <Wrapper>
        <Table>
          <thead>
            <tr>
              <th>#</th>
              <th>{t("Field.email")}</th>
              <th>{t("Field.role")}</th>
              <th>{t("Field.role")}</th>
              <th>{t("Field.password")}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {filteredUsers.map(u =>
              <User
                t={t}
                user={u}
                dispatch={dispatch}
                isAdmin={isUserAdmin(user)}
              />
            )}
          </tbody>
        </Table>
      </Wrapper>
    </RoutesWrapper>
  );
};

export default withTranslation(AdminUsersPage);

const Wrapper = styled.div`
  overflow: auto;
`;

const LoadingSpinnerInner = styled.i.attrs(props => ({
    className: "fas fa-spinner fa-spin"
}))``;