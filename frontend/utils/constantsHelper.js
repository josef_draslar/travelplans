export const userMutations = require("../generated/constants-shared/UserMutations").UserMutations;
export const userQueries = require("../generated/constants-shared/UserQueries").UserQueries;
export const tripMutations = require("../generated/constants-shared/TripMutations").TripMutations;
export const tripQueries = require("../generated/constants-shared/TripQueries").TripQueries;
export const constants = require("../generated/constants-shared/constants");