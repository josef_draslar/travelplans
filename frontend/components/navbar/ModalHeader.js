import React from "react";
import styled from "styled-components";
import {ModalHeader as ModalHeaderStrap} from "reactstrap";
import fonts from "../../utils/fonts";
import colors from "../../utils/colors";
import {useDispatch} from "react-redux";
import {fireLocalAction} from "../../redux/actions/LocalActions";
import {localActions} from "../../redux/reducers/LocalReducer";

export const ModalHeader = ({children}) => {
    const dispatch = useDispatch();
    const close = () => dispatch(fireLocalAction(localActions.CLOSE_JOIN));
    return (
        <ModalHeaderStyled toggle={close}>{children}</ModalHeaderStyled>
    )
};

const ModalHeaderStyled = styled(ModalHeaderStrap)`
  padding-top: 0;
  border-bottom: none;

  h5 {
    text-transform: uppercase;
    font-family: ${fonts.mainTitle};
    font-size: 2rem;
    font-weight: 500;
    line-height: 1.2;
    width: 100%;
    text-align: center;
  }

  button {
    position: absolute;
    top: -15px;
    right: -15px;
    color: ${colors.font};
    font-size: 30px;
    line-height: 10px;
  }
`;