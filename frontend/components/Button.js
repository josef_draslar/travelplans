import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Link from "next/link";
import colors from "../utils/colors";
import FONTS from "../utils/fonts";

const Button = ({
  onClick,
  href,
  children,
  backgroundColor,
  actionDone,
  inverseColors,
  color,
  hoverColor,
  displayBlock,
  inSiteLink,
  style,
  target,
  isButton,
  disabled,
  disabledColor,
  type,
  isLoading,
  iconKey,
  iconKeyFull
}) => {
  const iconKeyLocal = iconKey ? "fas fa-" + iconKey : iconKeyFull;
  return isButton
    ? <ButtonStyled
        onClick={onClick}
        backgroundColor={backgroundColor}
        color={color}
        hoverColor={hoverColor}
        displayBlock={displayBlock}
        style={style}
        target={target}
        disabled={disabled || isLoading}
        disabledColor={disabledColor}
        type={type}
        inverseColors={inverseColors}
      >
        <LeftPart inverseColors={inverseColors}>
          {children}
        </LeftPart>
        <RightPart>
          {isLoading
            ? <ButtonLeftSpinner />
            : <Span>
                <Arrow
                  iconKey={iconKeyLocal}
                  isLoading={isLoading}
                  actionDone={actionDone}
                />
                <Arrow
                  iconKey={iconKeyLocal}
                  isLoading={isLoading}
                  actionDone={actionDone}
                />
              </Span>}
        </RightPart>
      </ButtonStyled>
    : inSiteLink
      ? <Link href={href}>
          <A
            onClick={onClick}
            backgroundColor={backgroundColor}
            color={color}
            disabled={disabled || isLoading}
            hoverColor={hoverColor}
            displayBlock={displayBlock}
            inverseColors={inverseColors}
          >
            <LeftPart inverseColors={inverseColors}>
              {children}
            </LeftPart>
            <RightPart>
              {isLoading
                ? <ButtonLeftSpinner />
                : <Span>
                    <Arrow
                      iconKey={iconKeyLocal}
                      isLoading={isLoading}
                      actionDone={actionDone}
                    />
                    <Arrow
                      iconKey={iconKeyLocal}
                      isLoading={isLoading}
                      actionDone={actionDone}
                    />
                  </Span>}
            </RightPart>
          </A>
        </Link>
      : <A
          href={href}
          onClick={onClick}
          backgroundColor={backgroundColor}
          color={color}
          disabled={disabled || isLoading}
          hoverColor={hoverColor}
          displayBlock={displayBlock}
          style={style}
          target={target}
          inverseColors={inverseColors}
        >
          <LeftPart inverseColors={inverseColors}>
            {children}
          </LeftPart>
          <RightPart>
            {isLoading
              ? <ButtonLeftSpinner />
              : <Span>
                  <Arrow
                    iconKey={iconKeyLocal}
                    isLoading={isLoading}
                    actionDone={actionDone}
                  />
                  <Arrow
                    iconKey={iconKeyLocal}
                    isLoading={isLoading}
                    actionDone={actionDone}
                  />
                </Span>}
          </RightPart>
        </A>;
};

Button.propTypes = {
  onClick: PropTypes.func,
  href: PropTypes.string,
  backgroundColor: PropTypes.string,
  color: PropTypes.string,
  hoverColor: PropTypes.string,
  displayBlock: PropTypes.bool,
  inSiteLink: PropTypes.bool,
  isButton: PropTypes.bool,
  style: PropTypes.object,
  target: PropTypes.string,
  disabledColor: PropTypes.string,
  type: PropTypes.string,
  disabled: PropTypes.bool,
  isLoading: PropTypes.bool,
  actionDone: PropTypes.bool,
  inverseColors: PropTypes.bool,
  iconKey: PropTypes.string,
  iconKeyFull: PropTypes.string
};

Button.defaultProps = {
  onClick: () => {},
  style: {},
  color: colors.font,
  hoverColor: colors.middle,
  disabled: false,
  disabledColor: "gray"
};

export default Button;

const RightPart = styled.span`
  padding: 14px 13px;
  line-height: 14px;
  font-size: 16px;
  display: inline-block;
  z-index: 5;
  overflow: hidden;

  float: none;
  position: absolute;
  top: 50%;
  right: 0;
  margin-top: -22px;
`;

const Arrow = styled.i.attrs(props => ({
  className: props.isLoading
    ? "fas fa-spin fa-spinner"
    : props.actionDone
      ? "fas fa-check"
      : props.iconKey ? props.iconKey : "fas fa-long-arrow-alt-right"
}))`
  position: absolute;
  margin-left: -8px;
  &:first-child {
    margin-left: -28px;
  }

  -webkit-transition: all 0.25s linear;
  -moz-transition: all 0.25s linear;
  transition: all 0.25s linear;
`;

const Span = styled.span`
  line-height: 14px;
  width: 20px;
  height: 15px;
  font-size: 16px;
  position: relative;
  display: inline-block;

  overflow: hidden;
  padding: 0 8px !important;
`;

const LeftPart = styled.span`
  position: relative;
  padding: 14px 25px 16px;
  font-size: 12px;
  line-height: 14px;
  float: left;
  display: inline-block;
  margin-right: 48px;
  border-right: 1px solid
    ${props => (props.inverseColors ? colors.primary : colors.white)};
`;

const commonStyles = props => `
  padding-top: 0;
    padding-bottom: 0;
    color: ${props.inverseColors ? colors.primary : colors.white} !important;
    background-color: ${props.backgroundColor
      ? props.backgroundColor
      : props.inverseColors ? colors.white : colors.primary};
    
    border: 1px solid ${props.inverseColors ? colors.primary : "transparent"};
    
    font-family: ${FONTS.text};
    position: relative;
    display: inline-block;
    vertical-align: middle;
    width: auto;
    outline: 0;
    font-size: 14px;
    line-height: 18px;
    letter-spacing: 2px;
    font-weight: 700;
    text-transform: uppercase;
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    -webkit-transition: color .2s ease-in-out,background-color .2s ease-in-out,border-color .2s ease-in-out;
    -moz-transition: color .2s ease-in-out,background-color .2s ease-in-out,border-color .2s ease-in-out;
    transition: color .2s ease-in-out,background-color .2s ease-in-out,border-color .2s ease-in-out;
    
    text-decoration: none;
    
    ${props.disabled &&
      `pointer-events: none !important; background-color: ${colors.lighterGray};`}
    
    &:hover, &:focus {
      cursor: pointer;
      text-decoration: none;
      color: ${colors.white};
      background-color: ${colors.primaryHover};
    }
}
`;

const A = styled.a`
  ${props => commonStyles(props)};

  &:hover,
  &:focus {
    text-decoration: none;
    color: ${colors.white};
    background-color: ${colors.primaryHover};
    ${props =>
      props.backgroundColor && `background-color: ${props.backgroundColor};`};

    & > ${RightPart} > ${Span} > ${Arrow}:first-child {
      margin-left: -8px;
    }

    & > ${RightPart} > ${Span} > ${Arrow}:last-child {
      margin-left: 24px;
    }
    ${props => props.inverseColors && `border: 1px solid white !important;`};
  }
`;

const ButtonStyled = styled.button`
  ${props => commonStyles(props)};

  &:hover,
  &:focus {
    text-decoration: none;
    cursor: pointer;
    color: ${colors.white};
    background-color: ${colors.primaryHover};
    ${props =>
      props.backgroundColor && `background-color: ${props.backgroundColor};`};

    & > ${RightPart} > ${Span} > ${Arrow}:first-child {
      margin-left: -8px;
    }

    & > ${RightPart} > ${Span} > ${Arrow}:last-child {
      margin-left: 24px;
    }
    ${props => props.inverseColors && `border: 1px solid white !important;`};
  }
`;

const ButtonLeftSpinner = styled.i.attrs(props => ({
  className: "fas fa-spin fa-spinner"
}))`
  margin-right: 10px;
`;
