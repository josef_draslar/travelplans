import React from "react";
import PropTypes from "prop-types";
import {Modal} from "reactstrap";
import { connect } from "react-redux";
import styled from "styled-components";

import LogIn from "./LogIn";
import SignUp from "./SignUp";
import { fireLocalAction } from "../../redux/actions/LocalActions";
import {
  CHANGE_PASSWORD,
  RESET_PASSWORD,
  SIGN_IN,
  SIGN_UP
} from "../../redux/reducers/UserReducer";
import ResetPassword from "./ResetPassword";
import ResetPasswordChange from "./ResetPasswordChange";
import {localActions} from "../../redux/reducers/LocalReducer";
import {media} from "../../utils/Media";
import colors from "../../utils/colors";

class Join extends React.PureComponent {
  state = {
    screen: this.props.joinModal || SIGN_IN
  };

  static getDerivedStateFromProps(props, state) {
    if (state.index !== props.index) {
      return {
        ...state,
        remaining: props.initial,
        index: props.index,
        updatedByProps: true
      };
    }
  }

  openPasswordReset = () => this.setState({ screen: RESET_PASSWORD });

  openSignUp = () => this.setState({ screen: SIGN_UP });

  openSignIn = () => this.setState({ screen: SIGN_IN });

  getContent = () => {
    const { allowFacebook } = this.props;
    switch (this.state.screen) {
      case SIGN_IN:
        return (
          <LogIn
            swap={this.openSignUp}
            openPasswordReset={this.openPasswordReset}
            allowFacebook={allowFacebook}
          />
        );
      case SIGN_UP:
        return <SignUp swap={this.openSignIn} close={this.closeNotLoggedIn}
                       allowFacebook={allowFacebook} />;
      case RESET_PASSWORD:
        return <ResetPassword close={this.closeNotLoggedIn} />;
      case CHANGE_PASSWORD:
        return <ResetPasswordChange close={this.closeNotLoggedIn} />;
      default:
        return null;
    }
  };

  closeNotLoggedIn = () =>
    this.props.fireLocalAction(localActions.CLOSE_JOIN);

  render() {
    return (
      <ModalStyled isOpen={true} toggle={this.closeNotLoggedIn}>
        {this.getContent()}
      </ModalStyled>
    );
  }
}

Join.propTypes = {
  allowFacebook: PropTypes.bool
};

class JoinWrapper extends React.PureComponent {
  render() {
    const { joinModal } = this.props;

    if (!joinModal) return null;

    return <Join {...this.props} />;
  }
}

JoinWrapper.propTypes = {
    allowFacebook: PropTypes.bool
};

export default connect(
  state => ({
    joinModal: state.local.joinModal
  }),
  {
      fireLocalAction
  }
)(JoinWrapper);

export const ModalStyled = styled(Modal)`
  margin: 80px auto 30px auto;
  width: 750px;
  max-width: 80%;
  background-color: ${colors.white};
  padding: 32px;
  position: relative;

  &:hover {
    cursor: default;
  }

  .modal-content {
    border: none;
  }

  ${media.lessThan("sm")`
      max-width: 90%;
      margin-top: 20px;
    `};
`;