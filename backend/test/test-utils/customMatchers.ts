import {ExecutionResult} from "graphql/execution/execute";
import {Repository} from "typeorm";
import {User} from "../../src/entity/User";
import {Trip} from "../../src/entity/Trip";
import {
    ACCESS_DENIED, EMAIL_NOT_VALID, NOT_FOUND, PASSWORD_TOO_WEAK, START_DATE_AFTER_END, USER_EMAIL_NOT_EXIST,
    USER_EXIST_ERR,
    USER_PASSWORD_NOT_MATCH
} from "../../src/constants-shared/constants";

export const ERROR_MESSAGE = {
    ACCESS_DENIED: ACCESS_DENIED,
    INVALID_NULL: "got invalid value null",
    NOT_FOUND: NOT_FOUND,
    EMAIL_NOT_VALID: EMAIL_NOT_VALID,
    PASSWORD_TOO_WEAK: PASSWORD_TOO_WEAK,
    USER_EXIST_ERR: USER_EXIST_ERR,
    USER_EMAIL_NOT_EXIST: USER_EMAIL_NOT_EXIST,
    USER_PASSWORD_NOT_MATCH: USER_PASSWORD_NOT_MATCH,
    START_DATE_AFTER_END: START_DATE_AFTER_END
};

export const expectErrorMessage = (response: ExecutionResult) => (message: string) => {
    if(message===ERROR_MESSAGE.INVALID_NULL){
        message = expect.stringMatching(ERROR_MESSAGE.INVALID_NULL);
    }
    expect(response).toMatchObject({
        errors: [{message}]
    });
};

export const expectNumberOfDbInstances = (repo: Repository<User|Trip>) => async (count: number) =>
    expect((await repo.find()).length).toBe(count);