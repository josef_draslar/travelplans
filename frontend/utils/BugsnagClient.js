import React from "react";
import Bugsnag from '@bugsnag/js'
import BugsnagPluginReact from '@bugsnag/plugin-react'

Bugsnag.start({
    apiKey: 'dff2c12e736ac91ae934a2c254776aa2',
    plugins: [new BugsnagPluginReact(React)]
});

export default Bugsnag;