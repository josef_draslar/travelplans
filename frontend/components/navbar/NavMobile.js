import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import { media } from "../../utils/Media";
import { NavigationItem, Title } from "./MenuStyledComponents";
import colors from "../../utils/colors";
import fonts from "../../utils/fonts";
import {useDispatch, useSelector} from "react-redux";
import {fireLocalAction} from "../../redux/actions/LocalActions";
import {localActions} from "../../redux/reducers/LocalReducer";
import {withTranslation} from "../../utils/multiLanguageSupport";
import {logOutUser} from "../../redux/actions/UserActions";
import AdminNavBar from "./AdminNavBar";

const NavMobile = ({t, isOpen, toggle, isManager, isAdmin}) => {
    const dispatch = useDispatch();
    const openSignIn = () => {
      toggle();
      dispatch(fireLocalAction(localActions.OPEN_SIGN_IN));
    };
    const openSignUp = () => {
      toggle();
      dispatch(fireLocalAction(localActions.OPEN_SIGN_UP));
    };
    const logOut = () => {
      toggle();
      dispatch(logOutUser());
    };
    const user = useSelector(state => state.user.user);

  const stopPropagation = e => e.stopPropagation();
  const refresh = () => window.location.reload();

    return (
      <div>
        <BackDrop className={isOpen ? "isOpened" : ""} onClick={toggle} />
        <CollapseStyled
          className={isOpen ? "isOpened" : ""}
          navbar
          onClick={stopPropagation}
        >
          <CrossBars onClick={toggle} className={isOpen ? "" : "isClosed"}>
            <CrossBar />
            <CrossBar />
          </CrossBars>
          <Ul>
            <Title>MENU</Title>

            <NavigationItem url="/" name="main" onClick={toggle} isMobile isOpen={isOpen}/>
            <NavigationItem url="/trip/itinerary" name="itinerary_page" onClick={toggle} isMobile isOpen={isOpen}/>
              {!user && <NavigationItem
                      onClick={openSignIn}
                      name="signIn"
                       isMobile isOpen={isOpen}
                  />}
              {user
                  ? <NavigationItem
                      onClick={logOut}
                      name="logOut"
                      isMobile isOpen={isOpen}
                  />
                  : <NavigationItem
                      onClick={openSignUp}
                      name="signUp"
                       isMobile isOpen={isOpen}
                  />}
              {(isAdmin||isManager) && <AdminNavBar t={t} toggle={toggle} isOpen={isOpen} />}
          </Ul>
          <Refresh onClick={refresh}>
            <i className="fas fa-redo" /> {t("NavBar.refresh")}
          </Refresh>
        </CollapseStyled>
      </div>
    );
};

NavMobile.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  userToken: PropTypes.object,
  openSignUp: PropTypes.func.isRequired,
  toggle: PropTypes.func.isRequired,
  openSignIn: PropTypes.func.isRequired,
  logOutUser: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool,
  isManager: PropTypes.bool
};

export default withTranslation(NavMobile);

const CollapseStyled = styled.div`
  -webkit-transition: all 0.3s ease-out;
  -moz-transition: all 0.3s ease-out;
  transition: all 0.3s ease-out;

  background-color: ${colors.white};
  max-width: 360px;
  position: fixed;
  left: -90%;
  top: 0;
  bottom: 0;
  width: 90%;

  padding: 24px 0;

  &.isOpened {
    left: 0;
  }
`;

const BackDrop = styled.div`
  position: fixed;
  display: none;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  opacity: 0;
  background-color: ${colors.primaryBackground};

  -webkit-transition: opacity 2s ease-out;
  -moz-transition: opacity 2s ease-out;
  transition: opacity 2s ease-out;

  &.isOpened {
    display: block;
    opacity: 0.95;
  }

  &:hover {
    cursor: pointer;
  }
`;

const CrossBar = styled.div`
  background-color: ${colors.font};
  position: absolute;
  height: 2px;
  border-radius: 1px;
  width: 50%;
  top: 20px;
  left: 12px;

  &:first-child {
    transform: rotate(-45deg);
  }

  &:last-child {
    transform: rotate(45deg);
  }
`;

const CrossBars = styled.div`
  width: 40px;
  height: 40px;
  top: 15px;
  right: 15px;
  position: absolute;
  z-index: 1;
  ${media.lessThan("md")`
    top: 25px;
  `};

  -webkit-transition: opacity 0.3s ease-out;
  -moz-transition: opacity 0.3s ease-out;
  transition: opacity 0.3s ease-out;

  &:hover,
  &:focus {
    cursor: pointer;
  }

  &.isClosed {
    ${CrossBar}:first-child {
      opacity: 0;
    }

    ${CrossBar}:last-child {
      opacity: 0;
    }
  }

  display: block;
`;

const Refresh = styled.div`
  bottom: 15px;
  left: 15px;
  position: absolute;
  text-transform: uppercase;
  z-index: 1;
  color: ${colors.font};

  font-size: 17px;
  line-height: 23px;
  font-family: ${fonts.title};

  &:hover,
  &:focus {
    cursor: pointer;
  }

  i {
    padding-right: 10px;
  }
`;

const Ul = styled.ul`
  list-style: none;
  padding-right: 50px;
  margin-left: 0;
  padding-left: 10px;
`;