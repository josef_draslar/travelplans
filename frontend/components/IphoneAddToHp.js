import React from "react";
import styled from "styled-components";
import { getImageUrl } from "../utils/Utils";
import {ButtonLikeLink} from "./BasicElements";

class IphoneAddToHp extends React.PureComponent {
  state = {
    showInstallMessage: false
  };

  componentDidMount() {
    this.timeout = window.setTimeout(() => {
      // Detects if device is on iOS
      const isIos = () => {
        const userAgent = window.navigator.userAgent.toLowerCase();
        return /iphone|ipad|ipod/.test(userAgent);
      };

      // Detects if device is in standalone mode
      const isInStandaloneMode = () =>
        "standalone" in window.navigator && window.navigator.standalone;

      // Checks if should display install popup notification:
      if (isIos() && !isInStandaloneMode()) {
        this.setState({ showInstallMessage: true });
      }
    }, 4000);
  }

  componentWillUnmount() {
    window.clearTimeout(this.timeout);
  }

  refuse = () => {
    localStorage.setItem("installRefused", "true");

    this.setState({
      showInstallMessage: false
    });
  };

  render() {
    return null;
    if (!this.state.showInstallMessage) return null;

    if (localStorage.getItem("installRefused")) return null;

    return (
      <WrapperDiv>
        <Div>
          <ButtonLikeLink onClick={this.refuse}>&times;</ButtonLikeLink>
          <div>Získej tuto aplikaci:</div>
          <div>
            1. klikni níže na <Icon />
          </div>
          <div>
            2. <b>přidej aplikaci na domovskou stránku</b>
          </div>
          <span />
        </Div>
      </WrapperDiv>
    );
  }
}

export default IphoneAddToHp;

const Icon = styled.img.attrs(props => ({
    src: getImageUrl("ios-share-icon.png")
}))`
  width: auto;
  height: 1.1em;
`;

const WrapperDiv = styled.div`
  position: fixed;
  bottom: 20px;
  text-align: center;

  z-index: 1000;
  width: 100%;
  padding-left: 4px;
  padding-right: 4px;

  span {
    position: absolute;
    display: block;

    content: " ";
    left: 50%;
    width: 20px;
    height: 20px;
    background-color: #eee;
    margin-left: -10px;
    bottom: -10px;
    transform: rotate(45deg);

    z-index: -1;
  }
`;

const Div = styled.div`
  padding: 5px;

  background: #eee;

  color: #333;

  margin-left: 4px;
  margin-right: 4px;

  border-radius: 3px;

  a {
    float: right;
    padding-right: 6px;
    font-size: 24px;
    padding-left: 5px;
  }

  span {
    position: absolute;
    display: block;

    content: " ";
    left: 50%;
    width: 20px;
    height: 20px;
    background-color: #eee;
    margin-left: -10px;
    bottom: -10px;
    transform: rotate(45deg);

    z-index: -1;
  }
`;
