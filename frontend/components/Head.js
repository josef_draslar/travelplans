import { Head } from "next/document";

const name = "Travel planner";
const title = "Travel planner is the best travel planner.";
const desc = "Travel planner is the best travel planner. More description here.";
const imageUrl = "https://images.pexels.com/photos/672358/pexels-photo-672358.jpeg";
const url = "https://josef-travelplans.herokuapp.com/";

export default () =>
  <Head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1"
    />
    <script src="/lazysizes.min.js" async />
    <link rel="shortcut icon" href="/favicon.png" />
    <link rel="stylesheet" type="text/css" href="/static/nprogress.css" />
    <link
      href="https://fonts.googleapis.com/css2?family=Lato&family=Raleway&display=swap"
      rel="stylesheet"
    />
    <link
      href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
      crossOrigin="anonymous"
    />
    <script
      src="https://kit.fontawesome.com/defaa3d377.js"
      crossOrigin="anonymous"
      async
    />

    <link
      rel="apple-touch-icon"
      sizes="120x120"
      href="/static/favicon/apple-touch-icon.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="32x32"
      href="/static/favicon/favicon-32x32.png"
    />
    <link
      rel="icon"
      type="image/png"
      sizes="16x16"
      href="/static/favicon/favicon-16x16.png"
    />
    <link rel="manifest" href="/static/favicon/site.webmanifest" />
    <link
      rel="mask-icon"
      href="/static/favicon/safari-pinned-tab.svg"
      color="#5bbad5"
    />
    <link rel="shortcut icon" href="/static/favicon/favicon.ico" />
    <meta name="msapplication-TileColor" content="#209cee" />
    <meta
      name="msapplication-config"
      content="/static/favicon/browserconfig.xml"
    />
    <meta name="theme-color" content="#ffffff" />

    <title>{name}</title>


    <base href="/" />

    <meta name="description" content={desc} />
    <meta name="keywords" content="travel buddy trip road plane" />
    <meta name="image" content={imageUrl}/>

    <meta name="author" content="Josef Draslar" />
    <meta name="copyright" content="2020" />
    <meta name="application-name" content={name} />

    <meta name="referrer" content="origin"/>

    <meta itemprop="name" content={name}/>
    <meta itemprop="description" content={desc}/>
    <meta itemprop="image" content={imageUrl}/>

    <meta property="og:title" content={title}/>
    <meta property="og:description" content={desc}/>
    <meta property="og:image" content={imageUrl}/>
    <meta property="og:url" content={url}/>
    <meta property="og:site_name" content={name}/>
    <meta property="og:locale" content="en_EN"/>
    <meta property="fb:admins" content="1395157194147949"/>
    <meta property="og:type" content="website"/>
    <meta property="fb:app_id" content="2651189925021717" />

    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content={title} />
    <meta name="twitter:description" content={desc} />
    <meta name="twitter:image" content={imageUrl} />

  </Head>;
