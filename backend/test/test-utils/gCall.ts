import {graphql} from "graphql";
import {createSchema} from "../../src/utils/createSchema";
import {GraphQLSchema} from "graphql";
import {Context} from "../../src/auth/Context.interface";
import {ExecutionResult} from "graphql/execution/execute";

interface Options {
    source?: string,
    variableValues?: any
}

let schema: GraphQLSchema;

export const getGraphQL = async ({source, variableValues}: Options, contextValue: Context = {}): Promise<ExecutionResult> => {
    if(!schema){
        schema = await createSchema();
    }

    return graphql({
        schema,
        source,
        variableValues,
        contextValue
    })
};