import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import {withTranslation} from "../../utils/multiLanguageSupport";
import RoutesWrapper from "../../utils/RoutesWrapper";

const MustSignInBanner = withTranslation(({t, text}) => (
    <P>
        {text?text:t("Trip.welcome")}
    </P>
));

MustSignInBanner.propTypes = {
    text: PropTypes.string
};

export {MustSignInBanner};

const MustSignInPage = (props) => (
    <RoutesWrapper
        showHeaderShadow
    >
        <MustSignInBanner {...props} />
    </RoutesWrapper>
);

MustSignInPage.propTypes = {
  text: PropTypes.string
};

export default MustSignInPage;

const P = styled.p`
  padding: 40px;
  max-width: 600px;
  margin-left: auto;
  margin-right: auto;
  text-align: center;
`;