import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import faker from "faker";
import env from "dotenv";
import {
  User,
  UserCreate,
} from "../../src/entity/User";
import { UserMutations } from "../../src/constants-shared/UserMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const logInUser = async (
  expectedErrorMessage?: string,
  params: object = {},
  id: number = defaults.users.current.id
) => {
  const user: UserCreate = {
    email: defaults.users.current.email,
    password: defaults.users.current.password,
    ...params
  };
  const response: ExecutionResult = await getGraphQL({
    source: UserMutations.logInUser,
    variableValues: {
      data: user
    }
  });

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
  } else {
    expect(response.errors).not.toBeDefined();

    const userDb: User = await defaults.repos.user.findOne({
      where: { id },
      relations: ["trips"]
    });
    expect(userDb).toBeTruthy();

    expect(response).toMatchObject({
      data: {
        logInUser: {
          user: {
            id: userDb.id,
            email: userDb.email,
            role: "" + userDb.role,
            trips: userDb.trips.map(trip => ({
              destination: trip.destination,
              comment: trip.comment,
              id: trip.id,
              startDate: trip.startDate.toISOString(),
              endDate: trip.endDate.toISOString()
            }))
          },
          token: expect.stringMatching(/[^']{23,}?/)
        }
      }
    });
  }
};

describe("User login", () => {
  it("login User", async () => {
    const user = {
      email: faker.internet.email(),
      password: faker.internet.password(6, false, null, "aA")
    };
    const response: ExecutionResult = await getGraphQL({
      source: UserMutations.createUser,
      variableValues: {
        data: user
      }
    });
    await logInUser(null, user, response.data.createUser.user.id);
  });

  it("login User wrong password", async () => {
    await logInUser(ERROR_MESSAGE.USER_PASSWORD_NOT_MATCH);
  });

  it("login User non existing email", async () => {
    await logInUser(ERROR_MESSAGE.USER_EMAIL_NOT_EXIST, {
      email: faker.internet.email()
    });
  });
});