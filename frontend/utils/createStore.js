import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import rootReducer from "../redux/reducers/RootReducer";
import {refetchUser} from "../redux/actions/UserActions";

const store = createStore(rootReducer, {}, applyMiddleware(thunk));

store.dispatch(refetchUser());

export {store};