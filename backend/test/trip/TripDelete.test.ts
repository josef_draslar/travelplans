import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { Trip } from "../../src/entity/Trip";
import { ExecutionResult } from "graphql/execution/execute";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
  expectNumberOfDbInstances
} from "../test-utils/customMatchers";
import { TripMutations } from "../../src/constants-shared/TripMutations";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";

env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const deleteTrip = async (
  context?: Context,
  expectedErrorMessage?: string,
  id: number = defaults.trips.ofCurrent.id
) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: TripMutations.deleteTrip,
      variableValues: {
        id
      }
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
    expectNumberOfDbInstances(defaults.repos.trip)(defaults.users.count);
  } else {
    expect(response.errors).not.toBeDefined();
    expectNumberOfDbInstances(defaults.repos.trip)(defaults.users.count - 1);

    const tripDb: Trip = await defaults.repos.trip.findOne({ where: { id } });
    expect(tripDb).toBeFalsy();
  }
};

describe("Trip delete", () => {
  it("delete Trip no auth", async () => {
    await deleteTrip(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("delete Trip as user", async () => {
    await deleteTrip(defaults.contexts.user);
  });

  it("delete Trip as manager", async () => {
    await deleteTrip(defaults.contexts.manager, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("delete Trip not exist", async () => {
    await deleteTrip(
      defaults.contexts.user,
      ERROR_MESSAGE.NOT_FOUND,
      defaults.trips.ofCurrent.id + 100
    );
  });

  it("delete Trip of different user as user", async () => {
    await deleteTrip(
      defaults.contexts.user,
      ERROR_MESSAGE.ACCESS_DENIED,
      defaults.trips.ofDifferent.id
    );
  });

  it("delete Trip of different user as admin", async () => {
    await deleteTrip(
      defaults.contexts.admin,
      null,
      defaults.trips.ofDifferent.id
    );
  });
});
