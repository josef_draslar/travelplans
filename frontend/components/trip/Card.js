import React from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import Link from "next/link";
import Button from "../Button";
import colors from "../../utils/colors";
import { media } from "../../utils/Media";

const Card = ({ title, subTitle, subTitleSecond, text, btnText, url, as }) =>
  <Link href={url} as={as} key={title}>
    <CardStyled>
      <Title>
        {title}
      </Title>
      <SubTitleSecond>
        {subTitleSecond || null}
      </SubTitleSecond>
      <SubTitle>
        {subTitle}
      </SubTitle>
      <Text>
        {text}
      </Text>
      <Button isButton>
        {btnText}
      </Button>
    </CardStyled>
  </Link>;

Card.propTypes = {
  title: PropTypes.string.isRequired,
  subTitle: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  btnText: PropTypes.string.isRequired,
  subTitleSecond: PropTypes.string,
  url: PropTypes.string,
  as: PropTypes.string
};

export default Card;

const CardStyled = styled.div`
  display: block;
  padding: 20px;
  box-shadow: 0 0 30px 0 rgba(0, 0, 0, 0.15);
  max-width: 260px;
  width: 30%;
  display: inline-block;
  margin: 20px;
  text-align: left;

  cursor: pointer;
  color: ${colors.font};
  -webkit-transition: all 250ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
  transition: all 250ms cubic-bezier(0.445, 0.05, 0.55, 0.95);

  :-moz-focusring {
    outline: none;
  }

  &:hover,
  &:active,
  &:focus {
    color: ${colors.font};
    box-shadow: 0 30px 30px 0 rgba(0, 0, 0, 0.15);
    -webkit-transition: all 250ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
    transition: all 250ms cubic-bezier(0.445, 0.05, 0.55, 0.95);
    transform: translate(0, -4px);

    text-decoration: none;
    outline: none;
  }
  margin-bottom: 30px;

  ${media.lessThan("sm")`
    width: 100%;
  `};
`;

const Title = styled.div`
  font-size: 20px;
  line-height: 26px;
`;

const SubTitle = styled.div`
  margin-top: 10px;
  font-size: 12px;
  font-weight: 900;
  line-height: 9px;
`;

const SubTitleSecond = styled.div`
  margin-top: 10px;
  font-size: 12px;
  font-weight: 900;
  line-height: 9px;
  height: 9px;
`;

const Text = styled.div`
  margin-top: 10px;
  margin-bottom: 20px;
  height: 70px;
  overflow-y: hidden;
`;
