import { GraphQLClient } from 'graphql-request'

export const getGraphqlClient = authToken => new GraphQLClient('/graphql', {
    headers: {
        'Access-Control-Allow-Origin': '*',
        ...(authToken && { authorization: `Bearer ${authToken}` }),
    },
});