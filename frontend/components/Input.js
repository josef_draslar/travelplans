import React from "react";
import PropTypes from "prop-types";
import {FormGroup, Input as InputStrap} from "reactstrap";
import {LabelLocal} from "./join/ModalJoinElements";

const Input = ({name, type, value, onChange, label, placeholder, t}) => (
    <FormGroup>
        <LabelLocal htmlFor={name+"-field"}>
            {label?label:t?t("Field."+name):""}
        </LabelLocal>
        <InputStrap
            id={name+"-field"}
            type={type}
            name={name}
            value={value}
            onChange={onChange}
            placeholder={placeholder}

        />
    </FormGroup>
);

Input.propTypes = {
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
        PropTypes.instanceOf(Date)
    ]).isRequired,
    placeholder: PropTypes.string,
    type: PropTypes.string,
    object: PropTypes.func,
    label: PropTypes.string
};

Input.defaultProps = {
    type: "text",
};

export default Input;