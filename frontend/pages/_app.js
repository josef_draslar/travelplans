import React from "react";
import { fetchTranslations } from "../utils/multiLanguageSupport";
import ReactGA from "react-ga";
import { Provider } from "react-redux";
import { store } from "../utils/createStore";
import { createGlobalStyle } from "styled-components";
import colors from "../utils/colors";
import fonts from "../utils/fonts";
import Router from "next/router";
import NProgress from "nprogress";

Router.onRouteChangeStart = () => NProgress.start();

Router.onRouteChangeComplete = () => NProgress.done();

Router.onRouteChangeError = () => {
  console.error("route change error");
  NProgress.done();
};

const GlobalStyle = createGlobalStyle`
  html {
    font-size: 10px;
  }

  body {
    padding: 0;
    margin: 0;
    font-size: 1.5rem;
    line-height: 1.5;
    color: ${colors.font};
    font-family: ${fonts.text};
  }
  
  a {
    text-decoration: none;
    color: ${colors.font}
  }
  
  a:hover, a:focus {
    cursor: pointer;
  }
  
  button:focus {
    outline: none;
  }
`;

ReactGA.initialize("UA-145063756-1");

fetchTranslations();

function MyApp({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <React.Fragment>
        <GlobalStyle />
        <Component {...pageProps} />
      </React.Fragment>
    </Provider>
  );
}

export default MyApp;
