import React from "react";
import PropTypes from "prop-types";
import {NavigationItem, Title} from "./MenuStyledComponents";

const AdminNavBar = ({t,toggle,isOpen}) => (
        <div>
            <Title>{t("Admin.admin")}</Title>

            <NavigationItem url="/admin/users" name="admin_users_page" onClick={toggle} isMobile isOpen={isOpen}/>
        </div>
    );

AdminNavBar.propTypes = {
    t: PropTypes.object.isRequired,
    toggle: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired
};

export default AdminNavBar;