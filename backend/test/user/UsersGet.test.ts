import { closeConnection, testConnection } from "../test-utils/testConnection";
import { getGraphQL } from "../test-utils/gCall";
import env from "dotenv";
import { Context } from "../../src/auth/Context.interface";
import { initTestData, TestDataDefaults } from "../test-utils/initTestData";
import {
  ERROR_MESSAGE,
  expectErrorMessage,
} from "../test-utils/customMatchers";
import { ExecutionResult } from "graphql/execution/execute";
import { UserQueries } from "../../src/constants-shared/UserQueries";
env.config();

let defaults: TestDataDefaults;

beforeAll(async () => await testConnection(true));
afterAll(async () => await closeConnection());
beforeEach(async () => (defaults = await initTestData()));

const getUsers = async (context?: Context, expectedErrorMessage?: string) => {
  const response: ExecutionResult = await getGraphQL(
    {
      source: UserQueries.getUsers,
      variableValues: {}
    },
    context
  );

  if (expectedErrorMessage) {
    expectErrorMessage(response)(expectedErrorMessage);
  } else {
    expect(response.errors).not.toBeDefined();

    expect(response).toMatchObject({
      data: {
        getUsers: [
          {
            id: defaults.users.current.id,
            email: defaults.users.current.email,
            role: "" + defaults.users.current.role
          },
          {
            id: defaults.users.different.id,
            email: defaults.users.different.email,
            role: "" + defaults.users.different.role
          }
        ]
      }
    });

    expect(response.data.getUsers.length).toBe(defaults.users.count);
  }
};

describe("Users get", () => {
  it("get Users no auth", async () => {
    await getUsers(undefined, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("get Users as user", async () => {
    await getUsers(defaults.contexts.user, ERROR_MESSAGE.ACCESS_DENIED);
  });

  it("get Users as manager", async () => {
    await getUsers(defaults.contexts.manager, null);
  });

  it("get Users as admin", async () => {
    await getUsers(defaults.contexts.admin, null);
  });
});